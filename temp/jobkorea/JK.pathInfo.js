/*
    필수 참조 파일 : 
        JK.extend.js    --모듈 확장
        JK.regEx.js --정규식

*/
/*
    var extend = function() {
        var extended = {};
        for(key in arguments) {
            var argument = arguments[key];
            for (prop in argument) {
            if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                extended[prop] = argument[prop];
            }
            }
        }
        return extended;
    };
*/

//JK.pathInfo = extend((JK.pathInfo || {}), factory(JK.pathInfo));

JK.pathInfo = {
    //status : {"dev", "test", "stg", "pre", "real"};

    regEx : {
        dev: /^(dev)(.?)(jts)(.jobkorea.co.kr)$/i,
        tst: /^(jts)([0-9]+)?(.jobkorea.co.kr)$/i,
        stg: /^(stg)([0-9]+)?\-?(www)?(.jobkorea.co.kr)/i,
        pre: /^(www104.jobkorea.co.kr)/i
    },
    regExM : {
        dev: /^(dev)(.?)(mts)(.jobkorea.co.kr)$/i,
        tst: /^(mts)([0-9]+)?(.jobkorea.co.kr)$/i,
        stg: /^(stg)([0-9]+)?\-?(m)?(.jobkorea.co.kr)/i,
        pre: /^(m104.jobkorea.co.kr)/i
    },

    /**
    regEx : {
        dev: /^(dev)\-?(www)?(.albamon.com)$/i,
        tst: /^(test)\-?(www)?(.albamon.com)$/i,
        stg: /^(www19)(.albamon.com)$/i
    },
    regExM : {
        dev: /^(dev)\-?(m)?(.albamon.com)$/i,
        tst: /^(test)\-?(m)?(.albamon.com)$/i,
        stg: /^(m5)(.albamon.com)$/i
    }
     */

    //PC 사이트 여부
    isPc : function() {
        var realM = /^(m)([0-9]+)?(.jobkorea.co.kr)$/i;
        var domain = document.domain.toLowerCase();

        if (this.regExM.dev.test(domain) || this.regExM.tst.test(domain) 
            || this.regExM.stg.test(domain) || this.regExM.pre.test(domain)
            || (this.getStatus() === "real" && realM.test(domain))
        ) {
            return false;
        } else {
            return true;
        }
    },

    //site 상태반환
    getStatus : function() {
        var domain = document.domain.toLowerCase();
        if (this.regEx.dev.test(domain) || this.regExM.dev.test(domain)) {
            return "dev";
        } else if (this.regEx.tst.test(domain) || this.regExM.tst.test(domain)) {
            return "test";
        }  else if (this.regEx.stg.test(domain) || this.regExM.stg.test(domain)) {
            return "stg";
        }  else if (this.regEx.pre.test(domain) || this.regExM.pre.test(domain)) {
            return "pre";
        } else {
            return "real";
        }
    },

    //프로토콜 반환
    getProtocol : function () {
        var protocol = "http://";
        if ('http:' == document.location.protocol) {
            protocol = "http://";
        } else {
            protocol = "https://";
        }

        return protocol;
    },

    //PC 잡코리아 도메인 반환
    getPCDomain : function () {
        var status = this.getStatus();
        var domain = document.domain.toLowerCase();
        var hostname = "";
        var protocol = "http://";
        
        if (status === "local") {
            var matchs = this.regEx.dev.exec(domain);
            hostname = (matchs != null && typeof matchs[2] == ".") ? "dev.jts.jobkorea.co.kr" : "devjts.jobkorea.co.kr";
        } else if (status === "test") {
            var matchs = this.regEx.tst.exec(domain);
            hostname = (matchs != null && (typeof matchs[2] !== "undefined" && matchs[2] != "")) ? "jts" + matchs[2] + ".jobkorea.co.kr" : "jts.jobkorea.co.kr";
        } else if (status === "stg") {
            var matchs = this.regEx.stg.exec(domain);
            hostname = (matchs != null && (typeof matchs[2] !== "undefined" && matchs[2] != "")) ? "stg" + matchs[2] + "-www.jobkorea.co.kr" : "www104.jobkorea.co.kr";
        } else if (status === "pre") {
            hostname = "www104.jobkorea.co.kr";
        } else {
            hostname = "www.jobkorea.co.kr";
        }
        
        domain = this.getProtocol() + hostname;

        return domain;
    },

    //Mobile 잡코리아 도메인 반환
    getMobileDomain : function () {
        var status = this.getStatus();
        var domain = this.domain;
        var hostname = "";
        var protocol = "http://";
        
        if (status === "local") {
            var matchs = this.regExM.dev.exec(domain);
            hostname = (matchs != null && typeof matchs[2] == ".") ? "dev.mts.jobkorea.co.kr" : "devmts.jobkorea.co.kr";
        } else if (status === "test") {
            var matchs = this.regExM.tst.exec(domain);
            hostname = (matchs != null && (typeof matchs[2] !== "undefined" && matchs[2] != "")) ? "mts" + matchs[2] + ".jobkorea.co.kr" : "mts.jobkorea.co.kr";
        } else if (status === "stg") {
            var matchs = this.regExM.stg.exec(domain);
            hostname = (matchs != null && (typeof matchs[2] !== "undefined" && matchs[2] != "")) ? "stg" + matchs[2] + "-m.jobkorea.co.kr" : "m104.jobkorea.co.kr";
        } else if (status === "pre") {
            hostname = "m104.jobkorea.co.kr";
        } else {
            hostname = "m.jobkorea.co.kr";
        }
        
        domain = this.getProtocol() + hostname;

        return domain;
    }
};