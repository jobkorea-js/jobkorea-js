# 알바몬 JKMONI JS

알바몬 사이트에서 사용하는 공통 스크립트 JKMONI JS 입니다.
파일은 PC용과 모바일용으로 구분되며 자세한 사항은 아래와 같습니다.

## Feature

* moni.pc.js
    * 알바몬 PC사이트용 JKMONI JS 파일
* moni.m.js
    * 알바몬 모바일사이트용 JKMONI JS 파일

## Documentation

* [JKMONI JS(PC용) API 가이드](https://jobkorea-js.bitbucket.io/albamon/pc/) 바로가기 
* [JKMONI JS(모바일용) API 가이드](https://jobkorea-js.bitbucket.io/albamon/m/) 바로가기