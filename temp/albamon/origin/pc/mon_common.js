﻿/*알바몬 - 공통 js파일 */

// 유효한 메일주소형식인지 체크
function mail_check(mail) {
    var mailAddr = mail + "";
    if (mailAddr.length < 5) return false;
    else if (mailAddr.indexOf(".") != -1 && mailAddr.indexOf("@") != -1) {
        var ck = true;
        for (var i = 0; i < mailAddr.length; i++) {
            if ((mailAddr.charAt(i) == '_' || mailAddr.charAt(i) == '-') ||
			   (mailAddr.charAt(i) >= '0' && mailAddr.charAt(i) <= '9') ||
			   (mailAddr.charAt(i) >= 'A' && mailAddr.charAt(i) <= 'Z') ||
			   (mailAddr.charAt(i) >= 'a' && mailAddr.charAt(i) <= 'z') ||
			   (mailAddr.charAt(i) == '.' || mailAddr.charAt(i) == '@'))
                ;
            else {
                ck = false;
                break;
            }
        }
        return ck;
    }
}


// 분리형 이메일 입력 체크 함수
function mail_check_sp(mail) {
    if (mail == null) { return false; }
    var mailAddr = mail.split(" ").join();
    if (mailAddr == "") { return false; }

    var ck = false;
    for (var i = 0; i < mailAddr.length; i++) {
        if ((mailAddr.charAt(i) == '_' || mailAddr.charAt(i) == '-' || mailAddr.charAt(i) == '.') || (mailAddr.charAt(i) >= '0' && mailAddr.charAt(i) <= '9') || (mailAddr.charAt(i) >= 'A' && mailAddr.charAt(i) <= 'Z') || (mailAddr.charAt(i) >= 'a' && mailAddr.charAt(i) <= 'z')) {
            ck = true;
        } else {
            ck = false;
            break;
        }
    }
    return ck;
}

//새창
function new_win(a, b, c) {
    Mon_Popup_Position(a, "popDialog", b, c, "0");
}

//새창 - 주소창, 스크롤바 노츨
function new_win1(a, b, c) {
    Mon_Popup_Position(a, "popDialog", b, c, "1");
}

//새창 - 창이름
function swinPop(u, n, w, h, s) {
    var g_pop = window.open(u, n, 'menubar=0,toolbar=0,location=0,directories=0,resizable=0,scrollbars=' + s + ',status=0,width=' + w + ',height=' + h);
    g_pop.focus();
}

//숫자체크
function num_check(input_str) {
    if (input_str == null || input_str.length == 0) {
        return false;
    }
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9')) {; }
        else { return false; }
    }
    return true;
}

//영문자 체크
function alpa_check(input_str) {
    var result = true;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= 'A' && input_str.charAt(i) <= 'Z') ||
		   (input_str.charAt(i) >= 'a' && input_str.charAt(i) <= 'z')) {; }
        else {
            result = false;
            break;
        }
    }
}

//숫자, 영문자(대), 영문자(소), 한글 허용된특수문자
function subject_check(input_str) {
    var result = true;
    var nonchar = " -+#%()[]&,.'㈜㈔"; //허용된 특수문자 (넣고 테스트요망)
    var cnt = 0;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= 'A' && input_str.charAt(i) <= 'Z') || //대문자
			(input_str.charAt(i) >= 'a' && input_str.charAt(i) <= 'z') || //소문자
			(input_str.charAt(i) >= 'ㄱ' && input_str.charAt(i) <= '힣') || //한글
			(input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9') || //숫자
			(nonchar.indexOf(input_str.substring(i, i + 1)) > 0) || 	//혀용특수문자
			(input_str.charCodeAt(i) == 32) || //		space
			(input_str.charCodeAt(i) == 39) || //		'
			(input_str.charCodeAt(i) == 47)) {; //	/
        }
        else {
            result = false;
        }
    }
    return result
}

//숫자, 영문자(대), 영문자(소), 한글
function textcheck(input_str) {
    var result = true;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= 'A' && input_str.charAt(i) <= 'Z') || //대문자
			(input_str.charAt(i) >= 'a' && input_str.charAt(i) <= 'z') || //소문자
			(input_str.charAt(i) >= 'ㄱ' && input_str.charAt(i) <= '힣') || //한글
			(input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9')) {; //	/
        }
        else {
            result = false;
        }
    }
    return result
}


//문자열의 양쪽(왼쪽, 오른쪽) 공백을 제거 함수
function both_trim(a) {
    var search = 0
    while (a.charAt(search) == " ") {
        search = search + 1
    }
    a = a.substring(search, (a.length))
    search = a.length - 1
    while (a.charAt(search) == " ") {
        search = search - 1
    }
    return a.substring(0, search + 1)
}

//숫자에 컴마찍기
function commaNum(num) {
    var tmp_int = num;
    var tmp_int2 = tmp_int.length;
    tmp_str = new Array;
    var tmp_str2 = ""
    var tmp_int3 = eval(tmp_int2 - 4);
    var tmp_int4 = eval(tmp_int2 / 3);
    var tmp_int5 = parseInt(tmp_int4) * 3;

    for (i = 0 ; i < tmp_int2 ; i++) {
        tmp_str[i] = tmp_int.slice(i, i + 1);
        tmp_str2 = tmp_str2 + tmp_str[i];
        if (tmp_int3 > 2) {
            for (j = tmp_int5 ; j > -1 ; j = j - 3) {
                if (i == tmp_int3 - j) {
                    tmp_str2 = tmp_str2 + ",";
                }
            }
        } else {
            if (i == tmp_int3) {
                tmp_str2 = tmp_str2 + ",";
            }
        }
    }
    return tmp_str2;
}


//숫자에 컴마찍기(1000000,3) 3번째 자리에 콤마
function formatnumber(v1, v2) {
    var str = new Array();
    v1 = String(v1);
    for (var i = 1; i <= v1.length; i++) {
        if (i % v2) {
            str[v1.length - i] = v1.charAt(v1.length - i);
        } else {
            str[v1.length - i] = ',' + v1.charAt(v1.length - i);
        }
    }
    return str.join('').replace(/^,/, '');
}

//0~9 + -  만 입력 가능
function checkPhoneSelf(input_str) {
    var result = true;
    var nonchar = " +-"; //허용된 특수문자 (넣고 테스트요망)
    var cnt = 0;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9') || //숫자
			(nonchar.indexOf(input_str.substring(i, i + 1)) > 0)) 	//혀용특수문자
        { }
        else {
            result = false;
        }
    }
    return result
}
//전화번호 체크 정규식
function checkPhoneExp(phoneN)
{
    var regExp = /^01([0|1|6|7|8|9])-?([0-9]{3,4})-?([0-9]{4})$/;    
    if (!regExp.test(phoneN)) {
        alert("휴대폰번호만 입력 가능합니다.");
        return false
    } else 
    {
        return true;
    }
}

//전화번호 체크 정규식2
function chkCertPhone(Certphone) {
    var cert_flag = false;
    Certphone = Certphone.replace(/-/gi, "");
    if (num_check(Certphone)) {
        if (Certphone.length > 8) {
            var regExp = /^([0-9]{2,3})-?([0-9]{3,4})-?([0-9]{4})$/;
            if (regExp.test(Certphone)) {
                var regExp = /^(02|031|032|033|041|042|043|044|051|052|053|054|055|061|062|063|064|010|011|016|017|018|019|050|030|0130|060|070|080)/;
                if (regExp.test(Certphone)) {
                    cert_flag = true;
                }
            }
        } else {
            if (Certphone.length < 8) {
            } else {
                cert_flag = true;
            }
        }
    }
    return cert_flag;
}