﻿// 유효한 메일주소형식인지 체크
function mail_check(mail) {
    var mailAddr = mail + "";
    if (mailAddr.length < 5) return false;
    else if (mailAddr.indexOf(".") != -1 && mailAddr.indexOf("@") != -1) {
        var ck = true;
        for (var i = 0; i < mailAddr.length; i++) {
            if ((mailAddr.charAt(i) == '_' || mailAddr.charAt(i) == '-') ||
			   (mailAddr.charAt(i) >= '0' && mailAddr.charAt(i) <= '9') ||
			   (mailAddr.charAt(i) >= 'A' && mailAddr.charAt(i) <= 'Z') ||
			   (mailAddr.charAt(i) >= 'a' && mailAddr.charAt(i) <= 'z') ||
			   (mailAddr.charAt(i) == '.' || mailAddr.charAt(i) == '@'))
                ;
            else {
                ck = false;
                break;
            }
        }
        return ck;
    }
}


// 분리형 이메일 입력 체크 함수
function mail_check_sp(mail) {
    if (mail == null) { return false; }
    var mailAddr = mail.split(" ").join();
    if (mailAddr == "") { return false; }

    var ck = false;
    for (var i = 0; i < mailAddr.length; i++) {
        if ((mailAddr.charAt(i) == '_' || mailAddr.charAt(i) == '-' || mailAddr.charAt(i) == '.') || (mailAddr.charAt(i) >= '0' && mailAddr.charAt(i) <= '9') || (mailAddr.charAt(i) >= 'A' && mailAddr.charAt(i) <= 'Z') || (mailAddr.charAt(i) >= 'a' && mailAddr.charAt(i) <= 'z')) {
            ck = true;
        } else {
            ck = false;
            break;
        }
    }
    return ck;
}

//새창
function new_win(a, b, c) {
    Mon_Popup_Position(a, "popDialog", b, c, "0");
}

//새창 - 주소창, 스크롤바 노츨
function new_win1(a, b, c) {
    Mon_Popup_Position(a, "popDialog", b, c, "1");
}

//새창 - 창이름
function swinPop(u, n, w, h, s) {
    var g_pop = window.open(u, n, 'menubar=0,toolbar=0,location=0,directories=0,resizable=0,scrollbars=' + s + ',status=0,width=' + w + ',height=' + h);
    g_pop.focus();
}

//숫자체크
function num_check(input_str) {
    if (input_str == null || input_str.length == 0) {
        return false;
    }
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9')) {; }
        else { return false; }
    }
    return true;
}

//영문자 체크
function alpa_check(input_str) {
    var result = true;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= 'A' && input_str.charAt(i) <= 'Z') ||
		   (input_str.charAt(i) >= 'a' && input_str.charAt(i) <= 'z')) {; }
        else {
            result = false;
            break;
        }
    }
}

//숫자, 영문자(대), 영문자(소), 한글 허용된특수문자
function subject_check(input_str) {
    var result = true;
    var nonchar = " -+#%()[]&,.'㈜㈔"; //허용된 특수문자 (넣고 테스트요망)
    var cnt = 0;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= 'A' && input_str.charAt(i) <= 'Z') || //대문자
			(input_str.charAt(i) >= 'a' && input_str.charAt(i) <= 'z') || //소문자
			(input_str.charAt(i) >= 'ㄱ' && input_str.charAt(i) <= '힣') || //한글
			(input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9') || //숫자
			(nonchar.indexOf(input_str.substring(i, i + 1)) > 0) || 	//혀용특수문자
			(input_str.charCodeAt(i) == 32) || //		space
			(input_str.charCodeAt(i) == 39) || //		'
			(input_str.charCodeAt(i) == 47)) {; //	/
        }
        else {
            result = false;
        }
    }
    return result
}


//숫자, 영문자(대), 영문자(소), 한글 허용된특수문자
function mobile_check(input_str) {
    var result = true;
    var nonchar = " -+#%()[]&,.'㈜㈔!@#$%^&*{}\/<>;:|`~"; //허용된 특수문자 (넣고 테스트요망)
    var cnt = 0;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= 'A' && input_str.charAt(i) <= 'Z') || //대문자
			(input_str.charAt(i) >= 'a' && input_str.charAt(i) <= 'z') || //소문자
			(input_str.charAt(i) >= 'ㄱ' && input_str.charAt(i) <= '힣') || //한글
			(input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9') || //숫자
			(nonchar.indexOf(input_str.substring(i, i + 1)) > 0) || 	//혀용특수문자
			(input_str.charCodeAt(i) == 32) || //		space
			(input_str.charCodeAt(i) == 39) || //		'
			(input_str.charCodeAt(i) == 47)) {; //	/
        }
        else {
            result = false;
        }
    }
    return result
}

//숫자, 영문자(대), 영문자(소), 한글
function textcheck(input_str) {
    var result = true;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= 'A' && input_str.charAt(i) <= 'Z') || //대문자
			(input_str.charAt(i) >= 'a' && input_str.charAt(i) <= 'z') || //소문자
			(input_str.charAt(i) >= 'ㄱ' && input_str.charAt(i) <= '힣') || //한글
			(input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9')) {; //	/
        }
        else {
            result = false;
        }
    }
    return result
}


//문자열의 양쪽(왼쪽, 오른쪽) 공백을 제거 함수
function both_trim(a) {
    var search = 0
    while (a.charAt(search) == " ") {
        search = search + 1
    }
    a = a.substring(search, (a.length))
    search = a.length - 1
    while (a.charAt(search) == " ") {
        search = search - 1
    }
    return a.substring(0, search + 1)
}

//숫자에 컴마찍기
function commaNum(num) {
    var tmp_int = num;
    var tmp_int2 = tmp_int.length;
    tmp_str = new Array;
    var tmp_str2 = ""
    var tmp_int3 = eval(tmp_int2 - 4);
    var tmp_int4 = eval(tmp_int2 / 3);
    var tmp_int5 = parseInt(tmp_int4) * 3;

    for (i = 0 ; i < tmp_int2 ; i++) {
        tmp_str[i] = tmp_int.slice(i, i + 1);
        tmp_str2 = tmp_str2 + tmp_str[i];
        if (tmp_int3 > 2) {
            for (j = tmp_int5 ; j > -1 ; j = j - 3) {
                if (i == tmp_int3 - j) {
                    tmp_str2 = tmp_str2 + ",";
                }
            }
        } else {
            if (i == tmp_int3) {
                tmp_str2 = tmp_str2 + ",";
            }
        }
    }
    return tmp_str2;
}


//Label 클릭
function label_click(radio_id) {
    var obj = document.getElementById(radio_id);
    /*if(obj.disabled == false) {
		if(obj.checked){
			obj.checked = false;
		} else {
			obj.checked = true;
		}
	}*/
    return true;
}



var updateCharTimeLine = null;
function startUpdateChar(length_limit, _id) {
    window.clearInterval(updateCharTimeLine);
    updateCharTimeLine = window.setInterval(function () {
        updateChar(length_limit, _id);
    }, 400);
}

function stopUpdateChar() {
    window.clearInterval(updateCharTimeLine);
}


function updateChar(length_limit, _id) {
    var kp = $("#" + _id + "").val();
    var length = calculate_msglen(kp);

    if (length > length_limit) {
        alert("최대 " + length_limit + "자까지 입력할 수 있습니다.\n\n초과된 글자수는 자동으로 삭제됩니다. ");
        $("#" + _id + "").val(kp.replace(/\r\n$/, ""));
        $("#" + _id + "").val(assert_msglen(kp, length_limit))
        //		seq.value = seq.value.replace(/\r\n$/, "");
        //		seq.value = assert_msglen(kp, length_limit);
        length = length_limit;
    }

    var cntObj;
    if (_id == "M_Resume_Title") {
        cntObj = document.getElementById("dev_subject_cnt");
    } else if (_id == "M_Profile_Text") {
        cntObj = document.getElementById("dev_profile_cnt");
    } else if (_id == "dev_M_Award_Text") {
        cntObj = document.getElementById("dev_Award_cnt");
    } else if (_id == "dev_m_profile") {
        cntObj = document.getElementById("dev_M_Profile_cnt");
    } else {
        if (_id.indexOf("dev_C_Text") >= 0) {
            oID = _id.replace("dev_C_Text", "");
            cntObj = document.getElementById("dev_CareerTxt_cnt" + oID);
        }
    }

    if (kp != "") {

        length = commaNum("" + length + "");
        cntObj.innerHTML = length;
    } else {
        cntObj.innerHTML = "0";
    }
}



function calculate_msglen(message) {
    var nbytes = 0;
    for (i = 0; i < message.length; i++) {
        var ch = message.charAt(i);
        nbytes += 1;
    }
    return nbytes;
}


function assert_msglen(message, maximum) {
    var inc = 0;
    var nbytes = 0;
    var msg = "";
    var msglen = message.length;

    for (i = 0; i < msglen; i++) {
        var ch = message.charAt(i);
        inc = 1;
        if ((nbytes + inc) > maximum) {
            break;
        }
        nbytes += inc;
        msg += ch;
    }
    return msg;
}


function num_chk(pnum) {
    if (pnum.value.length > 0) {
        if (!num_check(pnum.value)) {
            alert("반드시 숫자로만 입력해야 합니다. ");
            pnum.value = "";
            pnum.focus();
            return;
        }
    }

}
function getByteLen(s) {
    var l = 0;
    for (var i = 0; i < s.length; i++)
        if (escape(s.charAt(i)).length > 3) l += 2; else l++;
    return l;
}



function getCookie(cookieName) {
    var search = cookieName + "=";
    var cookie = document.cookie;
    if (cookie.length > 0) {
        startIndex = cookie.indexOf(cookieName);
        if (startIndex != -1) {
            startIndex += cookieName.length;
            endIndex = cookie.indexOf(";", startIndex);

            if (endIndex == -1) endIndex = cookie.length;

            return unescape(cookie.substring(startIndex + 1, endIndex));
        } else {
            return "";
        }
    } else {
        return "";
    }
}

//특수문자 체크 숫자도 포함, 하이픈 -, 언더바 _ 포함.
function str_check_ID_SearchPWD(input) {
    var val = input;
    //var pattern = /[^가-힣ㄱ-ㅎㅏ-ㅣa-zA-Z 0-9_\- ]/;
    var pattern = /[^\w-]/;  //모든 영문자와 숫자 및 _-(언더바 하이픈)을 제외한 나머지 문자

    if (pattern.test(val)) {
        return false;
    }
    else {
        return true;
    }
}



/**
 * 쿠키 설정
 * @param cookieName 쿠키명
 * @param cookieValue 쿠키값
 * @param expireDay 쿠키 유효날짜
*/
function setCookie(cookieName, cookieValue, expireDate) {
    var today = new Date();
    today.setDate(today.getDate() + parseInt(expireDate));
    document.cookie = cookieName + "=" + escape(cookieValue) + "; path=/;domain=albamon.com;expires=" + today.toGMTString() + ";";
}

//쿠키 삭제
function deleteCookie(cookieName) {
    var expireDate = new Date();
    //어제 날짜를 쿠키 소멸 날짜로 설정한다.
    expireDate.setDate(expireDate.getDate() - 1);
    document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString() + ";path=/;domain=albamon.com;";
}

function returnVal(getVal, chk) {   //JSON Cookies 값 읽어오기
    var getValarr = JSON.parse(getVal);
    return getValarr[chk];
}

function returnValChg(getVal, chk, newval) {  //JSON Cookies 저장된 값 변경하기
    var getValarr = JSON.parse(getVal);
    delete getValarr[chk];
    getValarr[chk] = newval;
    return JSON.stringify(getValarr);
}
//////////////////////////////  쿠키값  //////////////////////////////



//////////////////////////// 로컬스토리지 ///////////////////////////
function returnJSONChg(name, attr, val) {  //값 변경
    var getVal = JSON.parse(localStorage[name]);
    delete getVal[attr];
    getVal[attr] = val;
    localStorage.setItem(name, JSON.stringify(getVal));
}

function JSONDel(name) {   //삭제
    localStorage.removeItem(name);
}

function returnJSON(name) {   //값 읽기
    return JSON.parse(localStorage[name]);
}

function setLocalStorage(name, val) {   //저장
    if (typeof localStorage === 'undefined') {
    } else {
        try {
            localStorage.setItem(name, val);
        } catch (e) {
            if (MobileType == "i") {
                alert("아이폰의 개인정보 보호 브라우징 기능으로 인해 서비스를 이용할 수 없습니다. \n[설정 - safari - 개인정보 보호 브라우징] 기능을 해제하신 후 다시 시도해주세요.");
                return;
            }
        }
    }
}

function getLocalStorage(name) {   //저장
    var getVal = "";
    if (typeof localStorage === 'undefined') {
    } else {
        try {
            getVal = localStorage.getItem(name);
        } catch (e) {
        }
    }
    return getVal;
}
//////////////////////////// 로컬스토리지 ///////////////////////////


function $focus(_id) {
    window.scroll(0, $("#" + _id + "").offset().top - 100);
}

function partNameChk(partcode, partname) {  //업직종의 코드값과 업직종명의 갯수 일치 여부 확인
    var partCArr = partcode.split(",");
    if (partCArr.length != partname.split(",").length) {   //넘어온 값의 갯수가 맞지 않으면 직접 만들어준다.
        partname = ""
        for (ik = 0; ik < partCArr.length; ik++) {
            if (partCArr[ik] != "") {
                for (ix = 0; ix < arrPartCode.length; ix++) {
                    for (iiu = 0; iiu < arrPartCode[ix].length; iiu++) {
                        if (arrPartCode[ix][iiu] == partCArr[ik]) {
                            partname = partname + arrPartName[ix][iiu] + ","
                        }
                    }
                }
            }
        }
        if (partname.substring(0, 1) != ",") { partname = "," + partname; }
    }
    return partname;
}




//////////////////////  IOS 전용  /////////////////////////
//아이폰 IOS 버전 비교
function iOSversion() {
    if (/iP(hone|od|ad)/.test(navigator.platform)) {
        // supports iOS 2.0 and later: <http://bit.ly/TJjs1V>
        var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
        return [parseInt(v[1], 10), parseInt(v[2], 10), parseInt(v[3] || 0, 10)];
    }
}

//아이폰 IOS 버전 비교
function IOSversionCompare(getVs) {
    var iOSversionstr = new String(getVs);
    var MobilVersion = makeVersion(iOSversionstr);
    MobilVersion = parseInt(MobilVersion, 0)
    return MobilVersion;
}
//아이폰 IOS 버전 비교
function makeVersion(getVs) {
    var makeMV = "";
    getVs = getVs.replace(/\./gi, ",");
    var arrMV = getVs.split(",");
    for (i = 0; i < arrMV.length; i++) {
        if (arrMV[i] > 9) {
            makeMV = makeMV + arrMV[i];
        } else {
            makeMV = makeMV + "0" + arrMV[i];
        }
    }
    return makeMV;
}
//IOSversionCompare("8.1.1");
//IOSversionCompare(iOSversion());   //모바일 버전
//////////////////////  IOS 전용  /////////////////////////


//oStr :URL 값,  sStr : 삭제하고픈 파라메타의 변수값
function saperate(oStr, sStr) {
    var retrueVal = oStr;
    //	alert("1" + retrueVal);
    retrueVal = eval("retrueVal.replace(/([&])" + sStr + "(.*?)&/gi,'&')");
    retrueVal = eval("retrueVal.replace(/([?])" + sStr + "(.*?)&/gi,'?')");
    retrueVal = eval("retrueVal.replace(/([&|?])" + sStr + "(.*)/gi,'')");
    //	alert("2" + retrueVal);
    return retrueVal;
}
//전화번호 체크 정규식
function checkPhoneExp(phoneN) {
    var regExp = /^01([0|1|6|7|8|9]?)-?([0-9]{3,4})-?([0-9]{4})$/;
    if (!regExp.test(phoneN)) {
        alert("휴대폰번호만 입력 가능합니다.");
        return false
    } else {
        return true;
    }
}

//전화번호 체크 정규식2
function chkCertPhone(Certphone) {
    var cert_flag = false;
    Certphone = Certphone.replace(/-/gi, "");
    if (num_check(Certphone)) {
        if (Certphone.length > 8) {
            var regExp = /^([0-9]{2,3})-?([0-9]{3,4})-?([0-9]{4})$/;
            if (regExp.test(Certphone)) {
                var regExp = /^(02|031|032|033|041|042|043|044|051|052|053|054|055|061|062|063|064|010|011|016|017|018|019|050|030|0130|060|070|080)/;
                if (regExp.test(Certphone)) {
                    cert_flag = true;
                }
            }
        } else {
            if (Certphone.length < 8) {
            } else {
                cert_flag = true;
            }
        }
    }
    return cert_flag;
}

//0~9 + -  만 입력 가능
function checkPhoneSelf(input_str) {
    var result = true;
    var nonchar = " +-"; //허용된 특수문자 (넣고 테스트요망)
    var cnt = 0;
    for (var i = 0; i < input_str.length; i++) {
        if ((input_str.charAt(i) >= '0' && input_str.charAt(i) <= '9') || //숫자
			(nonchar.indexOf(input_str.substring(i, i + 1)) > 0)) 	//혀용특수문자
        { }
        else {
            result = false;
        }
    }
    return result
}


var common_fn = {
    jsWriteChk: function (length_limit, seq, divn) {
        var comment = '';
        comment = document.getElementById(seq);
        var length = this.jsCharLen(comment.value);
        var ViewNum = length;
        if (Number(length) > Number(length_limit)) {

            alert("최대 " + length_limit + "자까지 입력할 수 있습니다.\n 초과된 글자수는 자동으로 삭제됩니다.");

            comment.value = comment.value.replace(/\r\n$/, "");
            comment.value = this.jsCharWrit(comment.value, length_limit, divn);

        } else {
            if (divn != "") {
                document.getElementById(divn).innerHTML = ViewNum;
            }
        }
    },
    jsCharLen: function (message) {
        var nbytes = 0;

        for (i = 0; i < message.length; i++) {
            var ch = message.charAt(i);
            if (escape(ch).length > 4) {
                nbytes += 1;
            } else if (ch == '\n') {
                if (message.charAt(i - 1) != '\r') {
                    nbytes += 0.5;
                }
            } else if (ch == '<' || ch == '>') {
                nbytes += 0.5;
            } else {
                nbytes += 0.5;
            }
        }
        return nbytes;
    },
    jsCharWrit: function (message, maximum, divn) {
        var inc = 0;
        var nbytes = 0;
        var msg = "";
        var msglen = message.length;

        for (i = 0; i < msglen; i++) {
            var ch = message.charAt(i);
            if (escape(ch).length > 4) {
                inc = 1;
            } else if (ch == '\n') {
                if (message.charAt(i - 1) != '\r') {
                    inc = 0.5;
                }
            } else if (ch == '<' || ch == '>') {
                inc = 0.5;
            } else {
                inc = 0.5;
            }
            if ((nbytes + inc) > maximum) {
                break;
            }
            nbytes += inc;
            msg += ch;
        }
        if (divn != "") {
            document.getElementById(divn).innerHTML = nbytes;
        }
        return msg;
    }
}