/**
 * moni.m.js 
 * @version 1.0.1
 * @author Jobkorea Development Lab
 * @etc required jquery
 * @dependency moni.m.js, core.js
 * jsdoc 생성 명령어 : jsdoc ./sources/albamon/moni.m.js ./sources/core/core.js -d ./docs/albamon/m/
 */
/**
 * 알바몬 메인 객체
 * @namespace moni
 */
(function(root, factory) {
    root.moni = factory(root);
})(this, function(root) {
    'use strict';

    var moni = {};

    /** 알바몬 공통 스크립트 버전 */
    moni.version = '1.0.1';

    var _moni = moni;
    /**
     * 타 라이브러리 충돌방지를 위한 alias 처리 (사용법 : var moni = moni.noConflict();)
     * @memberof moni
     * @returns {object} moni 객체
     */
    moni.noConflict = function() {
        return _moni;
    };

    /**
     * 네임스페이스 객체를 가져온다. (존재하지 않으면 생성)
     * @memberof moni
     * @param {string} _namespace 네임스페이스 문자
     * @returns {object} 호출된 네임스페이스 객체
     */
    moni.getNameSpace = function(_namespace) {
        var parts = _namespace.split('.'),
            parent = moni,
            i;
        if (parts[0] === 'moni') {
            parts = parts.slice(1);
        }
        for (i = 0; i < parts.length; i += 1) {
            // 프로퍼티가 존재하지 않으면 생성한다.
            if (typeof parent[parts[i]] === 'undefined') {
                parent[parts[i]] = {};
            }
            parent = parent[parts[i]];
        }
        return parent;
    };

    return moni;
});

/**
 * 알바몬 데이터 타입 체크 모듈
 * @namespace moni.type
 */
(function(root, factory) {
    root.moni.type = factory(root);
})(this, function(root) {
    'use strict';

    var type = moni.type || {};

    type = (function() {
        /**
         * 대상 데이터가 null 인지 검사한다.
         * 대상 데이터가 null 이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isNull
         * @param {*} obj 검사 대상 
         * @returns {boolean} null 여부
         * @example
         * moni.type.isNull(null);    // true
         * moni.type.isNull(1);       // false
         */
        var isNull = function(obj) {
            return obj === null;
        };
        /**
         * 대상 데이터가 undefined 인지 검사한다.
         * 대상 데이터가 undefined 이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isUndefined
         * @param {*} obj 검사 대상
         * @returns {boolean} undefined 여부
         * @example
         * moni.type.isUndefined(undefined);    // true
         * moni.type.isUndefined('undefined');  // false
         */
        var isUndefined = function(obj) {
            return typeof obj === 'undefined';
        };
        /**
         * 대상 데이터가 객체인지 검사한다.
         * 대상 데이터가 객체이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isObject
         * @param {*} obj 검사 대상
         * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
         * @returns {boolean} 객체 여부
         * @example
         * moni.type.isObject(new Object());   // true - new object
         * moni.type.isObject({});             // true - new object
         * moni.type.isObject('');             // false - new primitive string
         * moni.type.isObject(0);              // false - new primitive number
         * moni.type.isObject(false);          // false - new primitive boolean
         * moni.type.isObject([]);             // true - new array object
         * moni.type.isObject(function() {});  // true - new function object
         * moni.type.isObject(/()/);           // true - new regexp object
         * moni.type.isObject(null);           // false
         */
        var isObject = function(obj, isSafe) {
            if (isSafe) {
                return Object.prototype.toString.call(obj) === '[object Object]';
            }
            return obj === Object(obj);
        };
        /**
         * 대상 데이터가 배열 객체인지 검사한다.
         * 대상 데이터가 배열 이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isArray
         * @param {*} obj 검사 대상
         * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
         * @returns {boolean} 배열 여부
         * @example
         * moni.type.isArray(new Array());  // true
         * moni.type.isArray([]);           // true
         * moni.type.isArray(1);            // false
         */
        var isArray = function(obj, isSafe) {
            if (isSafe) {
                return Object.prototype.toString.call(obj) === '[object Array]';
            }
            return obj instanceof Array;
        };
        /**
         * 대상 데이터가 함수인지 검사한다.
         * 대상 데이터가 함수이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isFunction
         * @param {*} obj 검사 대상
         * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
         * @returns {boolean} 함수 여부
         * @example
         * moni.type.isFunction(new Function());  // true
         * moni.type.isFunction(function() {});   // true
         * moni.type.isFunction('function() {}'); // false
         */
        var isFunction = function(obj, isSafe) {
            if (isSafe) {
                return Object.prototype.toString.call(obj) === '[object Function]';
            }
            return typeof obj === 'function' || obj instanceof Function;
        };
        /**
         * 대상 데이터가 숫자인지 검사한다.
         * 대상 데이터가 숫자이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isNumber
         * @param {*} obj 검사 대상
         * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
         * @returns {boolean} 숫자 여부
         * @example
         * moni.type.isNumber(new Number());  // true
         * moni.type.isNumber(1);             // true
         * moni.type.isNumber('1');           // false
         */
        var isNumber = function(obj, isSafe) {
            if (isSafe) {
                return Object.prototype.toString.call(obj) === '[object Number]';
            }
            return typeof obj === 'number' || obj instanceof Number;
        };
        /**
         * 대상 데이터가 문자인지 검사한다.
         * 대상 데이터가 문자이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isString
         * @param {*} obj 검사 대상 
         * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
         * @returns {boolean} 문자 여부
         * @example
         * moni.type.isString(new String()); // true
         * moni.type.isString('1');          // true
         * moni.type.isString(1);            // false
         */
        var isString = function(obj, isSafe) {
            if (isSafe) {
                return Object.prototype.toString.call(obj) === '[object String]';
            }
            return typeof obj === 'string' || obj instanceof String;
        };
        /**
         * 대상 데이터가 Boolean인지 검사한다.
         * 대상 데이터가 Boolean이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isBoolean
         * @param {*} obj 검사 대상 
         * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
         * @returns {boolean} Boolean 여부
         * @example
         * moni.type.isBoolean(new Boolean()); // true
         * moni.type.isBoolean(true);          // true
         * moni.type.isBoolean(false);         // true
         * moni.type.isBoolean(0);             // false
         * moni.type.isBoolean(1);             // false
         * moni.type.isBoolean('true');        // false
         */
        var isBoolean = function(obj, isSafe) {
            if (isSafe) {
                return Object.prototype.toString.call(obj) === '[object Boolean]';
            }
            return typeof obj === 'boolean' || obj instanceof Boolean;
        };
        /**
         * 대상 데이터가 빈문자열인지 검사한다.
         * 대상 데이터가 빈문자열이면 true, 아니면 false를 반환한다.
         * @param {*} obj 검사 대상
         * @returns {boolean} 빈문자열 여부
         * @ignore
         */
        var _isEmptyString = function(obj) {
            return isString(obj) && obj === '';
        };
        /**
         * 대상 데이터가 agruments 객체인지 검사한다.
         * 대상 데이터가 agruments 이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isArguments
         * @param {*} obj 검사 대상
         * @returns {boolean} agruments 여부
         * @example
         * var fn = function(param1) {
         *      moni.type.isArguments(param1);               // false
         *      moni.type.isArguments(this.fn.arguments);    // true
         * };
         * fn(1, 2, 3);
         */
        var isArguments = function(obj) {
            return isExist(obj) && ((toString.call(obj) === '[object Arguments]') || !!obj.callee);
        };
        /**
         * 대상 데이터가 속성 객체인지 검사한다.
         * 대상 데이터가 속성 객체 이면 true, 아니면 false를 반환한다.
         * @param {*} obj 검사 대상
         * @returns {boolean} 속성 객체 여부
         * @ignore
         */
        var _hasOwnProperty = function(obj) {
            var key;
            for (key in obj) {
                if (obj.hasOwnProperty(key)) {
                    return true;
                }
            }
            return false;
        };
        /**
         * 대상 데이터가 존재하는지 검사한다.
         * 대상 데이터가 존재하면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isExist
         * @param {*} obj 검사 대상
         * @returns {boolean} 존재 여부
         * @example
         * moni.type.isExist('');        // true
         * moni.type.isExist(0);         // true
         * moni.type.isExist([]);        // true
         * moni.type.isExist({});        // true
         * moni.type.isExist(null);      // false
         * moni.type.isExist(undefined); // false
         */
        var isExist = function(obj) {
            return !isUndefined(obj) && !isNull(obj);
        };
        /**
         * 대상 데이터가 빈값인지 검사한다.
         * 대상 데이터가 빈값이면 true, 아니면 false를 반환한다.
         * @memberof moni.type
         * @method isEmpty
         * @param {*} obj 검사 대상
         * @returns {boolean} 반값 여부
         * @example
         * moni.type.isEmpty('');        // true
         * moni.type.isEmpty(0);         // false
         * moni.type.isEmpty([]);        // true
         * moni.type.isEmpty({});        // true
         * moni.type.isEmpty(null);      // true
         * moni.type.isEmpty(undefined); // true
         */
        var isEmpty = function(obj) {
            // 값존재 검사
            if (!isExist(obj) || _isEmptyString(obj)) {
                return true;
            }
            // 배열 검사
            if (isArray(obj) || isArguments(obj)) {
                return obj.length === 0;
            }
            // 속성 검사
            if (isObject(obj) && !isFunction(obj)) {
                return !_hasOwnProperty(obj);
            }
            return false;
        };
        
        return {
            isNull: isNull,
            isUndefined: isUndefined,
            isObject: isObject,
            isArray: isArray,
            isFunction: isFunction,
            isNumber: isNumber,
            isString: isString,
            isBoolean: isBoolean,
            isArguments: isArguments,
            isExist: isExist,
            isEmpty: isEmpty
        };
    })();

    return type;
});

/**
 * 알바몬 object 관련 객체
 * @namespace moni.object
 */
(function(root, factory) {
    root.moni.object = factory(root);
})(this, function(root) {
    'use strict';

    var obj = moni.object || {};

    obj = (function() {
        /**
         * 2개 이상의 객체를 첫번째 대상 객체로 병합(확장) 한다.
         * @memberof moni.object
         * @method extend
         * @param {object} target 대상 객체
         * @param {...object} objs 확장 할 객체들
         * @returns {object} 확정된 객체
         * @example
         * moni.object.extend(new Object(), new Object());
         */
        var extend = function (target, objs) {
            var hasOwnProp = Object.prototype.hasOwnProperty,
                i, len, argument, prop;

            for (i = 1, len = arguments.length; i < len; i += 1) {
                argument = arguments[i];
                if (argument != null) {
                    for (prop in argument) {
                        if (hasOwnProp.call(argument, prop)) {
                            target[prop] = argument[prop];
                        }
                    }
                }
            }

            return target;
        };

        return {
            extend: extend
        };
    })();

    return obj;
});

/**
 * 유효성 검사 모듈
 * @namespace moni.validator
 * @example
 * // 규칙정의
 * var rules = {
 *     title: '글제목',    // 메시지에 표시할 타이틀
 *     required: true,     // 필수여부 규칙
 *     max: { max: 10, message: '최대 10자를 넘었습니다.' },   // 최대값 검사 규칙
 *     min: { min: 5 }                                         // 최소값 검사 규칙
 * };
 * // 검사처리
 * moni.validator.go('', rules);
 * // Result { isValid: false, errors: ["글제목이(가) 필요합니다."], each: function }
 * moni.validator.go('안녕!', rules);
 * // Result { isValid: false, errors: ["글제목은(는) 최소5자 이상이어야 합니다"], each: function }
 * moni.validator.go('안녕하세요. 정말 좋은 아침입니다.', rules);   
 * // Result { isValid: false, errors: ["최대 10자를 넘었습니다."], each: function }
 * moni.validator.go('안녕하세요.', rules);                         
 * // Result { isValid: true, errors: [], each: function }  
 */
(function(root, factory) {
    root.moni.validator = factory(root);
})(this, function(root) {
    'use strict';

    var type = moni.type || {};
    var object = moni.object || {};

    if (type === null || typeof type === 'undefined') {
        return false;
    }
    if (object === null || typeof object === 'undefined') {
        return false;
    }

    var validator = moni.validator || {};

    /**
     * {@link moni.validator.go} 유효성검사 실행 결과 객체.<br>
     * 이 객체는 {@link moni.validator.go} 함수를 통해 사용되며, 검사 결과를 해당 객체로 받는다.
     * @memberof moni.validator
     * @typedef {object} result
     * @alias result
     * @property {boolean} isValid - 유효성 검사 실패 여부
     * @property {Array.<string>} errors - 유효성 실패 알림문구 목록
     * @property {function} each - 유효성검사 실패 알림문구를 호출하는 콜백 함수
     * @example
     * var rule = {
     *     title: '홈페이지 주소',
     *     url: { message: '올바른 URL 주소형식이 아닙니다.' },
     *     email: true,
     *     required: true,
     *     max: 5
     * };
     * var result = moni.validator.go('http://www.domain.com', rule);
     * // Result { isValid: true, errors: [], each: function }
     * rule.stop = false;   // 연속검사 설정
     * var result = moni.validator.go('domain', rule);
     * result.each(function (e) {
     *     console.log(e);
     * });
     * // 홈페이지 주소은(는) 최대5자 이하이어야 합니다.
     * // 홈페이지 주소은(는) 메일주소 형식이어야 합니다.
     * // 올바른 URL 주소형식이 아닙니다.
     */
    var Result = function() {
        this.isValid = true;
        this.errors = [];
        this.each = function (callback) {
            var isFunc = callback && callback.constructor && callback.call && callback.apply,
                i = this.errors.length;
            while (i--) {
                if (isFunc) {
                    callback(this.errors[i]);
                }
            }
        };
    };

    // 유효성검사 함수 확장
    object.extend(validator, Result, 
    //$.extend(validator, Result, 
        (function() {
            /**
             * 유효성 검사 시작
             * @ignore
             */
            var _start = function(value, _rules) {
                var result = new Result(),
                    title = '',
                    stop = true,
                    ignoreNull = false;
                // title 속성이 존재하면 담기
                if (_rules.hasOwnProperty('title')) {
                    title = _rules.title;
                }
                // ignoreNull 속성이 존재하면 담기
                if (_rules.hasOwnProperty('ignoreNull')) {
                    ignoreNull = _rules.ignoreNull;
                }
                // stop 속성이 존재하면 담기
                if (_rules.hasOwnProperty('stop')) {
                    stop = _rules.stop;
                }
                for (var rule in _rules) {
                    // 각 유효성검사에 실패했다면 추가검사 중지
                    if (stop && !result.isValid) {
                        break;
                    }
                    if (rules.hasOwnProperty(rule) && _isRule(rule)) {
                        // rule에 정의된 항목 값
                        var ruleValue = _rules[rule];
                        // rules 항목에 정의된 rule인지 확인
                        if (rules.hasOwnProperty(rule)) {
                            // 검사용 매개변수 생성
                            var params = {
                                ruleValue: ruleValue,
                                ruleName: rule,
                                title: title,
                                rule: rules[rule],
                                value: value,
                                ignoreNull: ignoreNull
                            };
                            // 유효성 검사 검사
                            _checkRule(params, result);
                        } else {
                            throw rule + ' 규칙이 정의되지 않았습니다.';
                        }
                    }
                }
                return result;
            };
            /**
             * 규칙에 대한 유효성검사를 수행하고 오류를 포함한 결과를 반환
             * @ignore
             */
            var _checkRule = function(params, result) {
                // null을 무시하는 경우
                if (params.hasOwnProperty('ignoreNull')) {
                    if (params.value === null && params.ignoreNull) {
                        return;
                    }
                }
                // 검사용 매개변수를 객체로 변환
                var _args = _getArgs(params);
                // 유효성검사 수행
                var _result = params.rule.validate(params.value, _args);

                result[params.ruleName] = {
                    isValid: true,
                    errors: []
                };
            
                // 유효성검사 결과값에 따른 result 객체 내 정보 담기
                if (typeof _result === 'object') {
                    // 유효성검사 성공여부 담기
                    //result.isValid = !_result.valid ? false : result.isValid;
                    result.isValid = _result.valid;
                    result[params.ruleName].isValid = _result.valid;
                    // 결과 객체에 따른 오류메시지 담기
                    if (_result.hasOwnProperty('errors')) {
                        var messages = this._formatMessages(_result.errors, params);
                        result.errors = result.errors.concat(messages);
                        if (typeof result[params.ruleName] !== 'undefined') {
                            result[params.ruleName].errors = messages;
                        }
                    }
                    // 결과 객체의 모든 속성을 반환 할 기본 result 객체와 병합
                    for (var prop in _result) {
                        if (_result.hasOwnProperty(prop) && !result.hasOwnProperty(prop)) {
                            result[params.ruleName][prop] = _result[prop];
                        }
                    }
                } else if (typeof _result !== 'boolean') {
                    throw '[ruleName : ' + params.ruleName + '] 올바르지 않는 결과값이 반환되었습니다.';
                } else {
                    //result.isValid = !_result ? false : result.isValid;
                    result.isValid = _result;
                    result[params.ruleName].isValid = _result;
                }
                // 유효성검사 실패 시 오류메시지 담기
                if (!result.isValid) {
                    var message = _formatMessage(params);
                    result.errors.push(message);
                    if (typeof result[params.ruleName] !== 'undefined') {
                        result[params.ruleName].errors.push(message);
                    }
                }
            };
            /**
             * 속성이 규칙인지 여부를 반환
             * @ignore
             */
            var _isRule = function(rule) {
                var props = [
                    'title',
                    'stop',
                    'ignoreNull'
                ];
                return props.indexOf(rule) < 0;
            };
            /**
             * 필요 매개변수 검사를 반복 처리
             * @ignore
             */
            var _eachExpected = function(params, fn) {
                if (Array.isArray(params.rule.expects)) {
                    var expectsLength = params.rule.expects.length,
                        i = expectsLength;
                    // 필요변수 검사
                    while (i--) {
                        fn(params.rule.expects[i], expectsLength);
                    }
                }
            };
            /**
             * 검사용 매개변수를 객체로 반환
             * @ignore
             */
            var _getArgs = function(params) {
                var pars = {};
                // validate 함수로 넘길 파라메터 객체 생성 (rule > expects에서 정의한 값을 토대로 생성함)
                _eachExpected(params, function (expects, expectsLength) {
                    if (params.ruleValue.hasOwnProperty(expects)) {
                        pars[expects] = params.ruleValue[expects];
                    } else if (expectsLength <= 1 && (/^[A-Za-z0-9]+$/i.test(params.ruleValue) || toString.call(params.ruleValue) === '[object RegExp]')) {
                        pars[expects] = params.ruleValue;
                    } else {
                        throw '[ruleName: ' + params.ruleName + '] ' + expects + ' parameter 가 필요합니다.';
                    }
                });

                if (params.ruleValue.hasOwnProperty('config')) {
                    pars.config = params.ruleValue.config;
                }
                return pars;
            };
            /**
             * 메시지 변환 함수
             * @ignore
             */
            var _format = function(text, col) {
                col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);
                return text.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
                    if (m === "{{") { return "{"; }
                    if (m === "}}") { return "}"; }
                    return col[n];
                }).trim();
            };
            /**
             * 오류 메시지의 형식을 올바르게 지정하기 위해 placholder 값을 포함하는 객체를 반환
             * @ignore
             */
            var _getFormat = function(params) {
                var format = {};
                // rule > expects에서 정의한 값이 있다면 validate 함수로 넘길 파라메터 객체 생성
                _eachExpected(params, function(expects) {
                    if (params.ruleValue.hasOwnProperty(expects)) {
                        format[expects] = params.ruleValue[expects];
                    }
                    if (/^[A-Za-z0-9]+$/i.test(params.ruleValue)) {
                        format[expects] = params.ruleValue;
                    }
                });
                format.title = params.title;
                return format;
            };
            /**
             * 유효성 검사 실패 시 메시지들을 반환
             * @ignore
             */
            var _formatMessages = function(errors, params) {
                var format = _getFormat(params),
                    i = errors.length;
                while (i--) {
                    errors[i] = _format(errors[i], format);
                }
                return errors;
            };
            /**
             * 유효성 검사 실패 시 메시지를 반환
             * @ignore
             */
            var _formatMessage = function(params) {
                var format = _getFormat(params),
                    message;
                // 미리 정의된 메시지가 있다면 메시지 담기
                if (params.ruleValue.hasOwnProperty('message')) {
                    message = params.ruleValue.message;
                    return _format(message, format);
                }
                else {
                    // 미리 정의된 메시지가 없다면 기본메시지로 담기
                    message = params.rule.message;
                    return _format(message, format);
                }
            };

            /**
             * {@link moni.validator.go} 유효성검사 실행 함수에 필요한 사용자정의 규칙 객체.<br>
             * 이 객체는 {@link moni.validator.go} 함수의 rules 인자값으로 사용된다.
             * @memberof moni.validator
             * @typedef {object} rules
             * @alias rules
             * @property {string} title - 규칙명. 검사실패 알림문구 중 {title}과 대체되어 표시 됨
             * @property {boolean} [stop=true] - 전체검사 여부<br>
             *                                   - true: 검사 실패 시 중단하고 결과를 Return<br>
             *                                   - false: 검사 실패와 관계없이 모든 규칙 검사 실행
             * @property {boolean} [ignoreNull=false] - null 무시 여부
             * 
             * @property {object|boolean} alpha - 대상 값이 영문 형식인지 검사
             *      @property {string} [alpha.message] - 사용자정의 검사실패 알림문구
             * @property {object|boolean} alphaNumeric - 대상 값이 영문 또는 숫자 형식인지 검사
             *      @property {string} [alphaNumeric.message] - 사용자정의 검사실패 알림문구
             * @property {object|string} date - 대상 값이 날짜 형식인지 검사 (정규식)
             *      @property {string} date.date - 검사 순서 ('ymd' : 년월일, 'dmy' : 일월년)
             *      @property {string} [date.message] - 사용자정의 검사실패 알림문구
             * @property {object|boolean} decimal - 대상 값이 소수 형식인지 검사
             *      @property {string} [decimal.message] - 사용자정의 검사실패 알림문구
             * @property {object|boolean} email - 대상 값이 메일 형식인지 검사
             *      @property {string} [email.message] - 사용자정의 검사실패 알림문구
             * @property {object} equal - 대상 값이 {value}값과 일치하는지 검사
             *      @property {string} equal.value - 비교대상 값
             *      @property {string} equal.field - 오류문구에 노출될 비교대상 값
             *      @property {string} [equal.message] - 사용자정의 검사실패 알림문구
             * @property {object} format - 대상 값을 정의된 정규식으로 검사
             *      @property {string} format.regex - 사용할 정규식
             *      @property {string} [format.message] - 사용자정의 검사실패 알림문구
             * @property {object|boolean} ip - 대상 값이 IP 형식인지 검사
             *      @property {string} [ip.message] - 사용자정의 검사실패 알림문구
             * @property {object|int} max - 대상 값이 {max}보다 큰 지 검사
             *      @property {int} max.max - 유효한 최대 값
             *      @property {string} [max.message] - 사용자정의 검사실패 알림문구
             * @property {object|int} min - 대상 값이 {min}보다 작은지 검사
             *      @property {int} min.min - 유효한 최소 값
             *      @property {string} [min.message] - 사용자정의 검사실패 알림문구
             * @property {object|boolean} numeric - 대상 값이 숫자 형식인지 검사
             *      @property {string} [numeric.message] - 사용자정의 검사실패 알림문구
             * @property {object} range - 대상 값이 {min}보다 작거나 {max}보다 큰 지 검사
             *      @property {int} range.min - 유효한 최대 값
             *      @property {int} range.max - 유효한 최소 값
             *      @property {string} [range.message] - 사용자정의 검사실패 알림문구
             * @property {object|boolean} required - 대상 값이 존재하는지 검사
             *      @property {string} [required.message] - 사용자정의 검사실패 알림문구
             * @property {object|boolean} time - 대상 값이 시간 형식인지 검사 (정규식)
             *      @property {string} [time.message] - 사용자정의 검사실패 알림문구
             * @property {object|boolean} url - 대상 값이 URL 형식인지 검사
             *      @property {string} [url.message] - 사용자정의 검사실패 알림문구
             * 
             * @example
             * //---------------------------------------
             * //   alpha rule
             * //---------------------------------------
             * var rules = {
             *      alpha: true
             * };
             * var result = moni.validator.go('alphabet', rules);
             * // Result { isValid: true, errors: [], each: function }
             * var rules = { 
             *      alpha: { message: '알파벳 형식만 가능합니다.' }
             * }
             * var result = moni.validator.go('alphabet123', rules);
             * // Result { isValid: true, errors: ["알파벳 형식만 가능합니다."], each: function }
             * 
             * //---------------------------------------
             * //   alphaNumeric rule
             * //---------------------------------------
             * var rules = {
             *      alphaNumeric: true
             * };
             * var result = moni.validator.go('alphabet 123', rules);
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   date rule
             * //---------------------------------------
             * var rules = {
             *      title: '등록일자',
             *      date: 'ymd'
             * };
             * var result = moni.validator.go('2017-08-08', rules);
             * // Result { isValid: true, errors: [], each: function }
             * var result = moni.validator.go('2017-08-00', rules);
             * // Result { isValid: true, errors: ["등록일자은(는) 날짜형식이어야 합니다."], each: function }
             * 
             * //---------------------------------------
             * //   decimal rule
             * //---------------------------------------
             * var rules = {
             *      decimal: true
             * };
             * var result = moni.validator.go('12.3', rules);
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   email rule
             * //---------------------------------------
             * var rules = {
             *      email: true
             * };
             * var result = moni.validator.go('user@domain.com', rules);
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   equal rule
             * //---------------------------------------
             * var rules = {
             *      title: '비밀번호',
             *      equal: {
             *          value: '1234',
             *          field: '12**'
             *      }
             * };
             * var result = moni.validator.go('1111', rules);
             * // Result { isValid: false, errors: ["비밀번호이(가) '12**'이(가) 일치하지 않습니다."], each: function }
             * var result = moni.validator.go('1234', rules);
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   format rule
             * //---------------------------------------
             * var rules = {
             *      title: '제목',
             *      format: {
             *          regex: /^[A-Za-z ]+$/
             *      }
             * };
             * var result = moni.validator.go('안녕하세요.', rules);
             * // Result { isValid: false, errors: ["제목이(가) 정규식[/^[A-Za-z ]+$/] 검사에 실패했습니다.], each: function }
             * var result = moni.validator.go('good morning', rules);
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   ip rule
             * //---------------------------------------
             * var rules = {
             *      ip: true
             * };
             * var result = moni.validator.go('192.168.0.1', rules);
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   max rule
             * //---------------------------------------
             * var rules = {
             *      max: 10
             * };
             * var result = moni.validator.go('안녕하세요. 정말 좋은 아침입니다.', rules);
             * // Result { isValid: false, errors: ["최대 10자를 넘었습니다."], each: function }
             * 
             * //---------------------------------------
             * //   min rule
             * //---------------------------------------
             * var rules = {
             *      min: 5
             * };
             * var result = moni.validator.go('안녕하세요. 반갑습니다.', rules);
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   numeric rule
             * //---------------------------------------
             * var rules = {
             *      numeric: true
             * };
             * var result = moni.validator.go('123', rules);
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   range rule
             * //---------------------------------------
             * var rules = {
             *     title: '글제목',    // 메시지에 표시할 타이틀
             *     range: { 
             *         min: 5,
             *         max: 10
             *     }
             * };
             * console.log(moni.validator.go('안녕', rules));
             * // Result { isValid: false, errors: ["글제목은(는) 최소5자 이상, 최대10자 이하이어야 합니다."], each: function }
             * console.log(moni.validator.go('안녕하세요. 반갑습니다. 감사합니다.', rules));
             * // Result { isValid: false, errors: ["글제목은(는) 최소5자 이상, 최대10자 이하이어야 합니다."], each: function }
             * console.log(moni.validator.go('안녕하세요.', rules));
             * // Result { isValid: true, errors: [], each: function }
             * 
             * //---------------------------------------
             * //   required rule
             * //---------------------------------------
             * var rules = {
             *      title: '제목',
             *      required: true
             * };
             * var result = moni.validator.go('value', rules);
             * // Result { isValid: false, errors: ["제목이(가) 필요합니다."], each: function }
             * 
             * //---------------------------------------
             * //   time rule
             * //---------------------------------------
             * var rules = {
             *      title: '등록시간',
             *      time: true
             * };
             * var result = moni.validator.go('10:59:59', rules);
             * // Result { isValid: true, errors: [], each: function }
             * var result = moni.validator.go('1:59:59', rules);
             * // Result { isValid: true, errors: [], each: function }
             *  var result = moni.validator.go('10:59:60', rules);
             * // Result { isValid: false, errors: ["등록시간은(는) 시간형식이어야 합니다."], each: function }
             * 
             * //---------------------------------------
             * //   url rule
             * //---------------------------------------
             * var rules = {
             *      url: { message: '올바른 URL 주소형식이 아닙니다.' }
             * };
             * var result = moni.validator.go('http://www.domain.com', rules);
             * // Result { isValid: true, errors: [], each: function }
             * var result = moni.validator.go('domain', rules);
             * // Result { isValid: false, errors: ["올바른 URL 주소형식이 아닙니다."], each: function }
             * var result = moni.validator.go('domain', {
             *     title: '홈페이지 주소',
             *     url: true
             * });
             * // Result { isValid: false, errors: ["홈페이지 주소은(는) URL 주소 형식이어야 합니다."], each: function }
             */
            var rules = {
                alpha: {
                    regex: /^[A-Za-z]+$/,
                    validate: function(value) {
                        return this.regex.test(value);
                    },
                    message: '{title}은(는) 영문이어야 합니다.',
                    expects: false
                },
                alphaNumeric: {
                    regex: /^[A-Za-z0-9]+$/i,
                    validate: function (value) {
                        return this.regex.test(value);
                    },
                    message: '{title}은(는) 영문 또는 숫자이어야 합니다.',
                    expects: false
                },
                date: {
                    formats: {
                        ymd: /^(?:\2)(?:[0-9]{2})?[0-9]{2}([\/-])(1[0-2]|0?[1-9])([\/-])(3[01]|[12][0-9]|0?[1-9])$/,
                        dmy: /^(3[01]|[12][0-9]|0?[1-9])([\/-])(1[0-2]|0?[1-9])([\/-])(?:[0-9]{2})?[0-9]{2}$/
                    },
                    validate: function (value, pars) {
                        return this.formats[pars.format].test(value);
                    },
                    message: '{title}은(는) 날짜형식이어야 합니다.',
                    expects: ['format']
                },
                decimal: {
                    regex: /^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/,
                    validate: function(value) {
                        return this.regex.test(value);
                    },
                    message: '{title}은(는) 소수이어야 합니다.',
                    expects: false
                },
                email: {
                    regex: /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i, // eslint-disable-line no-control-regex
                    validate: function(value) {
                        return this.regex.test(value);
                    },
                    message: '{title}은(는) 메일주소 형식이어야 합니다.',
                    expects: false
                },
                equal: {
                    validate: function(value, pars) {
                        return '' + value === '' + pars.value;
                    },
                    message: '{title}과(와) {field}이(가) 일치하지 않습니다.',
                    expects: ['value', 'field']
                },
                format: {
                    validate: function (value, pars) {
                        if (Object.prototype.toString.call(pars.regex) === '[object RegExp]') {
                            return pars.regex.test(value);
                        }
                        throw '[format] - regex is not a valid regular expression.';
                    },
                    message: '{title}이(가) 정규식[{regex}] 검사에 실패했습니다.',
                    expects: ['regex']
                },
                ip: {
                    regex: {
                        ipv4: /^(?:(?:\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(?:\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])$/,
                        ipv4Cidr: /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$/,
                        ipv6: /^((?=.*::)(?!.*::.+::)(::)?([\dA-F]{1,4}:(:|\b)|){5}|([\dA-F]{1,4}:){6})((([\dA-F]{1,4}((?!\3)::|:\b|$))|(?!\2\3)){2}|(((2[0-4]|1\d|[1-9])?\d|25[0-5])\.?\b){4})$/i,
                        ipv6Cidr: /^s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:)))(%.+)?s*(\/([0-9]|[1-9][0-9]|1[0-1][0-9]|12[0-8]))?$/
                    },
                    validate: function (value) {
                        return this.regex.ipv4.test(value) || this.regex.ipv6.test(value) || this.regex.ipv4Cidr.test(value) || this.regex.ipv6Cidr.test(value);
                    },
                    message: '{title}은(는) IP형식이어야 합니다.',
                    expects: false
                },
                max: {
                    validate: function(value, pars) {
                        return typeof value === 'string' && value.length <= pars.max;
                    },
                    message: '{title}은(는) 최대{max}자 이하이어야 합니다.',
                    expects: ['max']
                },
                min: {
                    validate: function(value, pars) {
                        return typeof value === 'string' && value.length >= pars.min;
                    },
                    message: '{title}은(는) 최소{min}자 이상이어야 합니다.',
                    expects: ['min']
                },
                numeric: {
                    regex: /^-?[0-9]+$/,
                    validate: function(value) {
                        return this.regex.test(value);
                    },
                    message: '{title}은(는) 숫자이어야 합니다.',
                    expects: false
                },
                range: {
                    validate: function(value, pars) {
                        if (typeof value === 'string')
                        {
                            return value.length >= pars.min && value.length <= pars.max;
                        } else if (typeof value === 'number') {
                            return value >= pars.min && value <= pars.max;
                        }
                        return false;
                    },
                    message: '{title}은(는) 최소{min}자 이상, 최대{max}자 이하이어야 합니다.',
                    expects: ['min', 'max']
                },
                required: {
                    validate: function(value) {
                        return !!value;
                    },
                    message: '{title}이(가) 필요합니다.',
                    expects: false
                },
                time: {
                    regex: /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$/,
                    validate: function (value) {
                        return this.regex.test(value);
                    },
                    message: '{title}은(는) 시간형식이어야 합니다.',
                    expects: false
                },
                url: {
                    regex: /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i,
                    validate: function(value) {
                        return this.regex.test(value);
                    },
                    message: '{title}은(는) URL 주소 형식이어야 합니다.',
                    expects: false
                }
            };
            /**
             * 유효성검사를 실행한다.
             * @memberof moni.validator
             * @method go
             * @param {*} value - 대상 값
             * @param {rules} rules - 유효성검사 규칙 객체
             * @returns {result} - 유효성검사 결과 객체
             * @example
             * // 규칙정의
             * var rules = {
             *     title: '글제목',    // 메시지에 표시할 타이틀
             *     required: true,     // 필수여부 규칙
             *     max: { max: 10, message: '최대 10자를 넘었습니다.' },   // 최대값 검사 규칙
             *     min: { min: 5 }                                         // 최소값 검사 규칙
             * };
             * // 검사처리
             * moni.validator.go('', rules);
             * // Result { isValid: false, errors: ["글제목이(가) 필요합니다."], each: function }
             * moni.validator.go('안녕!', rules);
             * // Result { isValid: false, errors: ["글제목은(는) 최소5자 이상이어야 합니다"], each: function }
             * moni.validator.go('안녕하세요. 정말 좋은 아침입니다.', rules);   
             * // Result { isValid: false, errors: ["최대 10자를 넘었습니다."], each: function }
             * moni.validator.go('안녕하세요.', rules);                         
             * // Result { isValid: true, errors: [], each: function }  
             */
            var go = function (value, rules) {
                if (typeof rules !== 'object') {
                    throw '유효성 검사에 필요한 규칙이 없습니다.';
                }
                // result 객체를 리턴
                return _start(value, rules);
            };
            /**
             * 사용자정의 규칙을 추가한다.
             * @memberof moni.validator
             * @method addRule
             * @param {object} rule 사용자정의 규칙 객체
             * @param {string} name 사용자정의 규칙명
             */
            var addRule = function(rule, name) {
                // 타입검사
                if (!type.isObject(rule)) {
                    throw '규칙 타입이 잘못 되었습니다.';
                }
                try {
                    // 규칙이 이미 존재하는지 검사
                    if (!rules.hasOwnProperty(name)) {
                        // 규칙이 이미 존재하지 않으면 추가
                        rules[name] = rule;
                    }
                } catch (e) {
                    throw 'moni.validator.addRule(): ' + e.message;
                }
            };

            return {
                rules: rules,
                go: go,
                addRule: addRule
            };
        })()
    );

    return validator;
});

/**
 * 알바몬 string 관련 모듈
 * @namespace moni.string
 */
(function(root, factory) {
    root.moni.string = factory(root);
})(this, function(root) {
    'use strict';

    var type = moni.type || {};
    var object = moni.object || {};

    if (type === null || typeof type === 'undefined') {
        return false;
    }
    if (object === null || typeof object === 'undefined') {
        return false;
    }

    var string = moni.string || {};

    string = (function() {
        /**
         * 문자열을 {cutLen}만큼 자른다.
         * @memberof moni.string
         * @method cut
         * @param {string} target 대상 문자
         * @param {int} cutLen 자를 길이
         * @returns {string} 결과 문자열
         * @example
         * moni.string.cut('abcdef', 2) // "ab"
         */
        var cut = function (target, cutLen) {
            var str = target,
                i = 0,
                length = target.length, 
                l = 0;

            for (i; i < length; i += 1) {
                var c = str.charCodeAt(i);
                l += c >> 11 ? 2 : c >> 7 ? 2 : 1;
                if (l > cutLen) return str.substring(0, i);
            }

            return str;
        };

        /**
         * 대상 문자열을 포멧형태로 변환한다. (C#의 String.Format과 유사)
         * @memberof moni.string
         * @method format
         * @param {string} target 대상 문자. {n}으로 변환할 위치를 지정한다.
         * @param {...*} args 치환할 문자들
         * @returns {string} 결과 문자열
         * @example
         * moni.string.format('{0}년{1}월', 2017, 8) // "2017년8월"
         */
        var format = function (target, args) {
            var str = target,
                i = 0,
                length = arguments.length;

            for (i; i < length; i+=1) {
                var regEx = new RegExp("\\{" + i + "\\}", "gm");
                var changeString = arguments[i+1];
                if (!type.isExist(changeString)) changeString = "";
                str = str.replace(regEx, changeString);
            }

            return str;
        };

        /**
         * 대상 문자열의 Byte 길이를 구한다.
         * @memberof moni.string
         * @method getByteSize
         * @param {string} target 대상 문자
         * @returns {int} 문자열의 Byte 길이
         * @example
         * moni.string.getByteSize('abcde') // 6
         * moni.string.getByteSize('한글')  // 6
         */
        var getByteSize = function (target) {
            var result;

            result = (function(s,b,i,c){
                for (b = i = 0; c = s.charCodeAt(i++); b += c >> 11 ? 3 : c >> 7 ? 2 : 1);
                return b;
            })(target);

            return result;
        };

        /**
         * 대상 문자열의 한글 포함여부를 가져온다.
         * @memberof moni.string
         * @method isKorean
         * @param {string} target 대상 문자
         * @returns {string} 한글 포함여부
         * @example
         * moni.string.isKorean('abcde') // false
         * moni.string.isKorean('abcde한글')  // true
         */
        var isKorean = function (target) {
            var iCode = 0, 
                cChar = "",
                i = 0,
                len = target.length;

            for (i; i < len; i+=1) {
                iCode = parseInt(target.charCodeAt(i));
                cChar = target.substr(i, 1).toUpperCase();
                if ((cChar < "0" || cChar > "9") && (cChar < "A" || cChar > "Z") && ((iCode > 255) || (iCode < 0))) {
                    return true;
                }
            }
            return false;
        };

        //날짜형식체크
        //"".isDate()
        var isDate = function () {
            var date = this.replace(/[^0-9]/, '');
            if (date.length == 8) {
                var year = Number(date.substring(0, 4));
                var month = Number(date.substring(4, 6));
                var day = Number(date.substring(6, 8));
            } else if (date.length == 6) {
                var year = Number('19' + date.substring(0, 2));
                var month = Number(date.substring(2, 4));
                var day = Number(date.substring(4, 6));
            } else { return false; }

            var rgMaxDay = [31, ((((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
            if (month < 1 || month > 12) { return false; }
            if (day < 1 || day > rgMaxDay[month - 1]) { return false; }
            return true;
        };

        //전체치환
        //"".replaceAll("/[a-z]/")
        var replaceAll = function () {
            var a = arguments, length = a.length;
            var regExp = new RegExp(a[0], "g");
            if (length == 0) { return this; }
            if (length == 1) { return this.replace(regExp, ""); }
            else { return this.replace(regExp, a[1]); }
            return this;
        };

        //정의된 태그를 제외한 나머지 태그들을 제거함
        //"".stripTag("<img><br><span>")
        var stripTag = function (allowed) {
            allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
            var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
            commentsAndTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
            return this.replace(commentsAndTags, '').replace(tags, function ($0, $1) {
                return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
            });
        };

        var trim = function (target) {
            return target.replace(/^\s+|\s+$/g, '');
        };

        var trunc = function(n, useWordBoundary) {
            var isTooLong = this.length > n,
                s_ = isTooLong ? this.substr(0, n) : this;
            s_ = (useWordBoundary && isTooLong) ? s_.substr(0, s_.lastIndexOf(' ')) : s_;
            return  isTooLong ? s_ + '...' : s_;
        };

        //모든태그제거 ex: "".tripTags()
        var stripTags = function () {
            return this.replace(/<.*?>/g, '');
        };

        //모든태그제거(text포함) ex: "".tripTagsAll()
        var stripTagsAll = function () {
            var s = this;
            s = s.replace(/<([^>]+?)([^>]*?)>(.*?)<\/\1>/ig, "");
            s = s.replace(/<([^>]+?)([^>]*?)>/ig, "");
            s = s.replace(/\s*$/g, "");

            return s;
        };

        //양쪽공백제거 ex: "".sideTrim()
        var sideTrim = function () { return this.replace(/(^\s*)|(\s*$)/g, ''); };

        //모든공백제거 ex: "".allTrim()
        var allTrim = function () { return this.replace(/\s*/g, ''); };

        //왼쪽공백제거 ex: "".leftTrim()
        var leftTrim = function () { return this.replace(/^\s*/g, ''); };

        //오른쪽공백제거 ex: "".rightTrim()
        var rightTrim = function () { return this.replace(/\s*$/g, ''); };

        //빈값체크 ex: "".isEmpty()
        var isEmpty = function () { return (this == null || this.trim() == '') ? true : false; };

        //왼쪽값채우기 ex: "1".lpad("0", 2), result: "01"
        var lpad = function (c, n) {
            var s = this;
            if (!s || !c || s.length >= n) {
                return s;
            }

            var max = (n - this.length) / c.length;
            for (var i = 0; i < max; i++) {
                s = c + s;
            }
            return s;
        };

        //오른쪽값채우기 ex: "1".rpad("0", 2), result: "10"
        var rpad = function (c, n) {
            var s = this;
            if (!s || !c || s.length >= n) {
                return s;
            }

            var max = (n - s.length) / c.length;
            for (var i = 0; i < max; i++) {
                s += c;
            }

            return s;
        };

        return {
            cut: cut,
            trim: trim,
            format: format,
            getByteSize: getByteSize,
            isKorean: isKorean
        };
    })();

    return string;
});