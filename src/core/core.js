/**
 * 전체 공통 스크립트 메인 객체
 * @author Jobkorea Development Lab
 * @version 1.0.1
 */
'use strict';

var core = {};

/** 전체 공통 스크립트 버전 */
core.version = '1.0.1';

/**
 * User Agent 정보
 * @namespace mon.browser
 * @type {Object}
 * @property {String} name 브라우저 이름
 * @property {String} version 브라우저 버전
 * @property {String} osname OS 이름
 * @property {String} osversion OS 버전
 * @property {String} info 브라우저 이름 버전 및 OS 이름 버전
 *
 * @property {Boolean} webkit 렌더링 엔진 체크
 * @property {Boolean} blink 렌더링 엔진 체크
 * @property {Boolean} gecko 렌더링 엔진 체크
 * @property {Boolean} msie 렌더링 엔진 체크
 * @property {Boolean} msedge 렌더링 엔진 체크
 *
 * @property {Boolean} mobile 디바이스 체크
 * @property {Boolean} tablet 디바이스 체크
 *
 * @property {Boolean} chrome 브라우저 체크
 * @property {Boolean} firefox 브라우저 체크
 * @property {Boolean} msie 브라우저 체크
 * @property {Boolean} msedge 브라우저 체크
 * @property {Boolean} safari 브라우저 체크
 * @property {Boolean} android 브라우저 체크
 * @property {Boolean} ios 브라우저 체크
 * @property {Boolean} opera 브라우저 체크
 * @property {Boolean} samsungBrowser 브라우저 체크
 * @property {Boolean} phantom 브라우저 체크
 * @property {Boolean} blackberry 브라우저 체크
 * @property {Boolean} webos 브라우저 체크
 * @property {Boolean} silk 브라우저 체크
 * @property {Boolean} bada 브라우저 체크
 * @property {Boolean} tizen 브라우저 체크
 * @property {Boolean} seamonkey 브라우저 체크
 * @property {Boolean} sailfish 브라우저 체크
 * @property {Boolean} ucbrowser 브라우저 체크
 * @property {Boolean} qupzilla 브라우저 체크
 * @property {Boolean} vivaldi 브라우저 체크
 * @property {Boolean} sleipnir 브라우저 체크
 * @property {Boolean} kMeleon 브라우저 체크
 *
 * @property {Boolean} mac OS 체크
 * @property {Boolean} windows OS 체크
 * @property {Boolean} windowsphone OS 체크
 * @property {Boolean} linux OS 체크
 * @property {Boolean} chromeos OS 체크
 * @property {Boolean} android OS 체크
 * @property {Boolean} ios OS 체크
 * @property {Boolean} blackberry OS 체크
 * @property {Boolean} firefoxos OS 체크
 * @property {Boolean} webos OS 체크
 * @property {Boolean} bada OS 체크
 * @property {Boolean} tizen OS 체크
 * @property {Boolean} sailfish OS 체크
 *
 * @example
 * .browser // Object {name: "Chrome", chrome: true, version: "59.0", blink: true, windows: true…}
 */
core.browser = (function() {
    var ua = typeof navigator !== 'undefined' ? navigator.userAgent || '' : '';

    function getFirstMatch(regex) {
        var match = ua.match(regex);
        return (match && match.length > 1 && match[1]) || '';
    }

    function getSecondMatch(regex) {
        var match = ua.match(regex);
        return (match && match.length > 1 && match[2]) || '';
    }

    var iosdevice = getFirstMatch(/(ipod|iphone|ipad)/i).toLowerCase(),
        likeAndroid = /like android/i.test(ua),
        android = !likeAndroid && /android/i.test(ua),
        nexusMobile = /nexus\s*[0-6]\s*/i.test(ua),
        nexusTablet = !nexusMobile && /nexus\s*[0-9]+/i.test(ua),
        chromeos = /CrOS/.test(ua),
        silk = /silk/i.test(ua),
        sailfish = /sailfish/i.test(ua),
        tizen = /tizen/i.test(ua),
        webos = /(web|hpw)os/i.test(ua),
        windowsphone = /windows phone/i.test(ua),
        samsungBrowser = /SamsungBrowser/i.test(ua),
        windows = !windowsphone && /windows/i.test(ua),
        mac = !iosdevice && !silk && /macintosh/i.test(ua),
        linux = !android && !sailfish && !tizen && !webos && /linux/i.test(ua),
        edgeVersion = getFirstMatch(/edge\/(\d+(\.\d+)?)/i),
        versionIdentifier = getFirstMatch(/version\/(\d+(\.\d+)?)/i),
        tablet = /tablet/i.test(ua),
        mobile = !tablet && /[^-]mobi/i.test(ua),
        xbox = /xbox/i.test(ua),
        result;
    var osVersion = '';

    if (/opera/i.test(ua)) {
        //  an old Opera
        result = {
            name: 'Opera',
            opera: true,
            version: versionIdentifier || getFirstMatch(/(?:opera|opr|opios)[\s\/](\d+(\.\d+)?)/i)
        };
    } else if (/opr|opios/i.test(ua)) {
        // a new Opera
        result = {
            name: 'Opera',
            opera: true,
            version: getFirstMatch(/(?:opr|opios)[\s\/](\d+(\.\d+)?)/i) || versionIdentifier
        };
    } else if (/SamsungBrowser/i.test(ua)) {
        result = {
            name: 'Samsung Internet for Android',
            samsungBrowser: true,
            version: versionIdentifier || getFirstMatch(/(?:SamsungBrowser)[\s\/](\d+(\.\d+)?)/i)
        };
    } else if (/coast/i.test(ua)) {
        result = {
            name: 'Opera Coast',
            coast: true,
            version: versionIdentifier || getFirstMatch(/(?:coast)[\s\/](\d+(\.\d+)?)/i)
        };
    } else if (/yabrowser/i.test(ua)) {
        result = {
            name: 'Yandex Browser',
            yandexbrowser: true,
            version: versionIdentifier || getFirstMatch(/(?:yabrowser)[\s\/](\d+(\.\d+)?)/i)
        };
    } else if (/ucbrowser/i.test(ua)) {
        result = {
            name: 'UC Browser',
            ucbrowser: true,
            version: getFirstMatch(/(?:ucbrowser)[\s\/](\d+(?:\.\d+)+)/i)
        };
    } else if (/mxios/i.test(ua)) {
        result = {
            name: 'Maxthon',
            maxthon: true,
            version: getFirstMatch(/(?:mxios)[\s\/](\d+(?:\.\d+)+)/i)
        };
    } else if (/epiphany/i.test(ua)) {
        result = {
            name: 'Epiphany',
            epiphany: true,
            version: getFirstMatch(/(?:epiphany)[\s\/](\d+(?:\.\d+)+)/i)
        };
    } else if (/puffin/i.test(ua)) {
        result = {
            name: 'Puffin',
            puffin: true,
            version: getFirstMatch(/(?:puffin)[\s\/](\d+(?:\.\d+)?)/i)
        };
    } else if (/sleipnir/i.test(ua)) {
        result = {
            name: 'Sleipnir',
            sleipnir: true,
            version: getFirstMatch(/(?:sleipnir)[\s\/](\d+(?:\.\d+)+)/i)
        };
    } else if (/k-meleon/i.test(ua)) {
        result = {
            name: 'K-Meleon',
            kMeleon: true,
            version: getFirstMatch(/(?:k-meleon)[\s\/](\d+(?:\.\d+)+)/i)
        };
    } else if (windowsphone) {
        result = {
            name: 'Windows Phone',
            windowsphone: true
        };
        if (edgeVersion) {
            result.msedge = true;
            result.version = edgeVersion;
        } else {
            result.msie = true;
            result.version = getFirstMatch(/iemobile\/(\d+(\.\d+)?)/i);
        }
    } else if (/msie|trident/i.test(ua)) {
        result = {
            name: 'Internet Explorer',
            msie: true,
            version: getFirstMatch(/(?:msie |rv:)(\d+(\.\d+)?)/i)
        };
    } else if (chromeos) {
        result = {
            name: 'Chrome',
            chromeos: true,
            chromeBook: true,
            chrome: true,
            version: getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
        };
    } else if (/chrome.+? edge/i.test(ua)) {
        result = {
            name: 'Microsoft Edge',
            msedge: true,
            version: edgeVersion
        };
    } else if (/vivaldi/i.test(ua)) {
        result = {
            name: 'Vivaldi',
            vivaldi: true,
            version: getFirstMatch(/vivaldi\/(\d+(\.\d+)?)/i) || versionIdentifier
        };
    } else if (sailfish) {
        result = {
            name: 'Sailfish',
            sailfish: true,
            version: getFirstMatch(/sailfish\s?browser\/(\d+(\.\d+)?)/i)
        };
    } else if (/seamonkey\//i.test(ua)) {
        result = {
            name: 'SeaMonkey',
            seamonkey: true,
            version: getFirstMatch(/seamonkey\/(\d+(\.\d+)?)/i)
        };
    } else if (/firefox|iceweasel|fxios/i.test(ua)) {
        result = {
            name: 'Firefox',
            firefox: true,
            version: getFirstMatch(/(?:firefox|iceweasel|fxios)[ \/](\d+(\.\d+)?)/i)
        };
        if (/\((mobile|tablet);[^\)]*rv:[\d\.]+\)/i.test(ua)) {
            result.firefoxos = true;
        }
    } else if (silk) {
        result = {
            name: 'Amazon Silk',
            silk: true,
            version : getFirstMatch(/silk\/(\d+(\.\d+)?)/i)
        };
    } else if (/phantom/i.test(ua)) {
        result = {
            name: 'PhantomJS',
            phantom: true,
            version: getFirstMatch(/phantomjs\/(\d+(\.\d+)?)/i)
        };
    } else if (/slimerjs/i.test(ua)) {
        result = {
            name: 'SlimerJS',
            slimer: true,
            version: getFirstMatch(/slimerjs\/(\d+(\.\d+)?)/i)
        };
    } else if (/blackberry|\bbb\d+/i.test(ua) || /rim\stablet/i.test(ua)) {
        result = {
            name: 'BlackBerry',
            blackberry: true,
            version: versionIdentifier || getFirstMatch(/blackberry[\d]+\/(\d+(\.\d+)?)/i)
        };
    } else if (webos) {
        result = {
            name: 'WebOS',
            webos: true,
            version: versionIdentifier || getFirstMatch(/w(?:eb)?osbrowser\/(\d+(\.\d+)?)/i)
        };
    } else if (/bada/i.test(ua)) {
        result = {
            name: 'Bada',
            bada: true,
            version: getFirstMatch(/dolfin\/(\d+(\.\d+)?)/i)
        };
    } else if (tizen) {
        result = {
            name: 'Tizen',
            tizen: true,
            version: getFirstMatch(/(?:tizen\s?)?browser\/(\d+(\.\d+)?)/i) || versionIdentifier
        };
    } else if (/qupzilla/i.test(ua)) {
        result = {
            name: 'QupZilla',
            qupzilla: true,
            version: getFirstMatch(/(?:qupzilla)[\s\/](\d+(?:\.\d+)+)/i) || versionIdentifier
        };
    } else if (/chromium/i.test(ua)) {
        result = {
            name: 'Chromium',
            chromium: true,
            version: getFirstMatch(/(?:chromium)[\s\/](\d+(?:\.\d+)?)/i) || versionIdentifier
        };
    } else if (/chrome|crios|crmo/i.test(ua)) {
        result = {
            name: 'Chrome',
            chrome: true,
            version: getFirstMatch(/(?:chrome|crios|crmo)\/(\d+(\.\d+)?)/i)
        };
    } else if (android) {
        result = {
            name: 'Android',
            version: versionIdentifier
        };
    } else if (/safari|applewebkit/i.test(ua)) {
        result = {
            name: 'Safari',
            safari: true
        };
        if (versionIdentifier) {
            result.version = versionIdentifier;
        }
    } else if (iosdevice) {
        result = {
            name : iosdevice == 'iphone' ? 'iPhone' : iosdevice == 'ipad' ? 'iPad' : 'iPod'
        };
        // WTF: version is not part of user agent in web apps
        if (versionIdentifier) {
            result.version = versionIdentifier;
        }
    } else if(/googlebot/i.test(ua)) {
        result = {
            name: 'Googlebot',
            googlebot: true,
            version: getFirstMatch(/googlebot\/(\d+(\.\d+))/i) || versionIdentifier
        };
    } else {
        result = {
            name: getFirstMatch(/^(.*)\/(.*) /),
            version: getSecondMatch(/^(.*)\/(.*) /)
        };
    }

    // set webkit or gecko flag for browsers based on these engines
    if (!result.msedge && /(apple)?webkit/i.test(ua)) {
        if (/(apple)?webkit\/537\.36/i.test(ua)) {
            result.name = result.name || "Blink";
            result.blink = true;
        } else {
            result.name = result.name || "Webkit";
            result.webkit = true;
        }
        if (!result.version && versionIdentifier) {
            result.version = versionIdentifier;
        }
    } else if (!result.opera && /gecko\//i.test(ua)) {
        result.name = result.name || "Gecko";
        result.gecko = true;
        result.version = result.version || getFirstMatch(/gecko\/(\d+(\.\d+)?)/i);
    }

    // set OS flags for platforms that have multiple browsers
    if (!result.windowsphone && !result.msedge && (android || result.silk)) {
        result.android = true;
        result.osname = 'Android';
    } else if (!result.windowsphone && !result.msedge && iosdevice) {
        result[iosdevice] = true;
        result.ios = true;
        result.osname = iosdevice;
    } else if (mac) {
        result.mac = true;
        result.osname = 'Mac';
    } else if (xbox) {
        result.xbox = true;
        result.osname = 'Xbox';
    } else if (windows) {
        result.windows = true;
        result.osname = 'Windows';
    } else if (linux) {
        result.linux = true;
        result.osname = 'Linux';
    }

    function getWindowsVersion(s) {
        switch (s) {
            case 'NT': return 'NT';
            case 'XP': return 'XP';
            case 'NT 5.0': return '2000';
            case 'NT 5.1': return 'XP';
            case 'NT 5.2': return '2003';
            case 'NT 6.0': return 'Vista';
            case 'NT 6.1': return '7';
            case 'NT 6.2': return '8';
            case 'NT 6.3': return '8.1';
            case 'NT 10.0': return '10';
            default: return undefined;
        }
    }

    // OS version extraction
    if (result.windows) {
        osVersion = getWindowsVersion(getFirstMatch(/Windows ((NT|XP)( \d\d?.\d)?)/i));
    } else if (result.windowsphone) {
        osVersion = getFirstMatch(/windows phone (?:os)?\s?(\d+(\.\d+)*)/i);
    } else if (result.mac) {
        osVersion = getFirstMatch(/Mac OS X (\d+([_\.\s]\d+)*)/i);
        osVersion = osVersion.replace(/[_\s]/g, '.');
    } else if (iosdevice) {
        osVersion = getFirstMatch(/os (\d+([_\s]\d+)*) like mac os x/i);
        osVersion = osVersion.replace(/[_\s]/g, '.');
    } else if (android) {
        osVersion = getFirstMatch(/android[ \/-](\d+(\.\d+)*)/i);
    } else if (result.webos) {
        osVersion = getFirstMatch(/(?:web|hpw)os\/(\d+(\.\d+)*)/i);
    } else if (result.blackberry) {
        osVersion = getFirstMatch(/rim\stablet\sos\s(\d+(\.\d+)*)/i);
    } else if (result.bada) {
        osVersion = getFirstMatch(/bada\/(\d+(\.\d+)*)/i);
    } else if (result.tizen) {
        osVersion = getFirstMatch(/tizen[\/\s](\d+(\.\d+)*)/i);
    }
    if (osVersion) {
        result.osversion = osVersion;
    }

    // device type extraction
    var osMajorVersion = !result.windows && osVersion.split('.')[0];
    if (tablet || nexusTablet || iosdevice == 'ipad' || (android && (osMajorVersion == 3 || (osMajorVersion >= 4 && !mobile))) || result.silk) {
        result.tablet = true;
    } else if (mobile || iosdevice == 'iphone' || iosdevice == 'ipod' || android || nexusMobile || result.blackberry || result.webos || result.bada) {
        result.mobile = true;
    }

    result.info = result.osname + ' ' + result.osversion + ', ' + result.name + ' ' + result.version;

    function getMajorVersion(version) {
        return version.split(".")[0];
    }

    /**
     * body 객체에 class명을 생성해주는 함수
     * @memberof mon
     * @function bodyAddClass
     * @param {Number} [option=browser&OS class add] 1:browser class add, 2: OS class add
     * @example
     * .browser.bodyAddClass(1);
     * // body add class name : is-ie , is-ie8 , is-chrome , is-firefox
     * .browser.bodyAddClass(2);
     * // body add class name : is-windows , is-windows7 , is-android , is-android5 , is-ios , is-ios9
     */
    result.bodyAddClass = function(option) {
        var classNames = '';
        if ((option == null)||(option == 1)) {
            if ((result.msie)||(result.msedge)) {
                classNames += ' is-ie';
                classNames += ' is-ie'+getMajorVersion(result.version);
            } else if (result.chrome) {
                classNames += ' is-chrome';
            } else if (result.firefox) {
                classNames += ' is-firefox';
            }
        }
        if ((option == null)||(option == 2)) {
            if (result.windows) {
                classNames += ' is-windows';
                classNames += ' is-windows'+getMajorVersion(result.osversion);
            } else if (result.android) {
                classNames += ' is-android';
                classNames += ' is-android'+getMajorVersion(result.osversion);
            } else if (result.ios) {
                classNames += ' is-ios';
                classNames += ' is-ios'+getMajorVersion(result.osversion);
            }
        }
        document.getElementsByTagName('body')[0].className += classNames;

        return classNames;
    };

    return result;
})();

/**
 * lazy 정보
 * @namespace mon.lazy
 * @type {Object}
 * @property {Function} detach image lazy load function - scroll event 제거 함수
 * @property {Function} image image lazy load function
 * @property {Function} js js lazy load function
 * @property {Function} jsPromise js lazy load function - Promise방식
 * @property {Function} render image lazy load function - scroll 시 영역 render 함수
 * @property {Function} renderAll image lazy load function - page load 시 모든 image load하는 함수
 * @property {Function} youTube youTube lazy load function
 */
core.lazy = (function() {
    var lazy = {};
    var root = window;
    var callback = function() {};
    var errorSrc = 'http://i.jobkorea.kr/content/images/m/noneimg/blank.png';// errorSrc Default
    var exclusion = 'lazy-exclusion'; // exclusion class name Default
    var offset, poll, delay, useDebounce, unload;

    var isHidden = function(element) {
        return (element.offsetParent === null);
    };

    var hasSomeParentTheClass = function(element, className) {
        var regex = new RegExp('\\b' + className + '\\b');
        do {
            if (regex.exec(element.className)) {
                return true;
            }
            element = element.parentNode;
        } while (element);
        return false;
    };

    var inView = function(element, view) {
        if (isHidden(element)) {
            return false;
        }
        var box = element.getBoundingClientRect();
        return (box.right >= view.l && box.bottom >= view.t && box.left <= view.r && box.top <= view.b);
    };

    var isExclusion = function(element) {
        return !hasSomeParentTheClass(element, exclusion);
    };

    var debounceOrThrottle = function() {
        if(!useDebounce && !!poll) {
            return;
        }
        clearTimeout(poll);
        poll = setTimeout(function() {
            lazy.render();
            poll = null;
        }, delay);
    };

    var imgError = function(elem) {
        elem.onerror = function() {
            // var src = elem.getAttribute('src');
            elem.src = errorSrc;
        };
    };

    var imgRender = function(elem) {
        var src;
        if (elem.getAttribute('data-bg-original') !== null) {
            elem.style.backgroundImage = 'url(' + elem.getAttribute('data-bg-original') + ')';
        } else if (elem.src !== (src = elem.getAttribute('data-original'))) {
            elem.src = src;
        }

        var classNames = (" " + elem.className + " ").replace(/[\n\t]/g, " ");
        if ( classNames.indexOf(" lazyBg ") > -1 ) {
            elem.className = classNames.replace(" lazyBg ", " ");
        }

        if (!unload) {
            elem.removeAttribute('data-original');
            elem.removeAttribute('data-bg-original');
        }
        callback(elem);
        imgError(elem);
    };

    /**
     * image lazy load(scroll type) : 스크롤에 따라 지연한 이미지를 불러오는 함수
     * @memberof mon.lazy
     * @function image
     * @param {Object} options options
     * @param {Number} [options.offset=0] 값이 0이면 이미지요소가 뷰포트에 표시되면 이미지를 로드합니다. 특정 값(ex 1500)을 넣으면, 뷰포트 위 또는 아래 특정 값(1500)부터 이미지를 로드합니다.
     * @param {String} [options.errorSrc='http://i.jobkorea.kr/content/images/m/noneimg/blank.png'] 이미지 로드에 실패하였을 경우 불러올 에러 이미지 경로
     * @param {String} [options.exclusion='lazy-exclusion'] lazy load 제외할 dom의 class name. (ex swipe area)
     * @param {Function} [options.callback] callback 함수
     * @example
     * .lazy.image({
     *     offset: 1500,
     *     errorSrc: 'http://i.jobkorea.kr/content/images/m/....../error_image.png',
     *     callback: function (element, op) {
     *         console.log(element, 'has been loaded')
     *     }
     * });
     */
    lazy.image = function(opts) {
        opts = opts || {};
        var offsetAll = opts.offset || 0;// offset Default: 0
        var offsetVertical = opts.offsetVertical || offsetAll;
        var offsetHorizontal = opts.offsetHorizontal || offsetAll;
        var optionToInt = function(opt, fallback) {
            return parseInt(opt || fallback, 10);
        };
        offset = {
            t: optionToInt(opts.offsetTop, offsetVertical),
            b: optionToInt(opts.offsetBottom, offsetVertical),
            l: optionToInt(opts.offsetLeft, offsetHorizontal),
            r: optionToInt(opts.offsetRight, offsetHorizontal)
        };
        delay = optionToInt(opts.throttle, 50); // throttle Default: 50
        useDebounce = opts.debounce !== false;
        unload = !!opts.unload;
        callback = opts.callback || callback;
        errorSrc = opts.errorSrc || errorSrc;
        exclusion = opts.exclusion || exclusion;
        lazy.render();
        if (root.addEventListener) {
            root.addEventListener('scroll', debounceOrThrottle, false);
            root.addEventListener('load', debounceOrThrottle, false);
        } else {
            root.attachEvent('onscroll', debounceOrThrottle);
            root.attachEvent('onload', debounceOrThrottle);
        }
    };

    /**
     * image lazy load(all image load type) : page load 이후 모든 이미지를 불러오는 함수
     * @memberof mon.lazy
     * @function renderAll
     * @param {Object} options options
     * @param {String} [options.errorSrc='http://i.jobkorea.kr/content/images/m/noneimg/blank.png'] 이미지 로드에 실패하였을 경우 불러올 에러 이미지 경로
     * @param {String} [options.exclusion='lazy-exclusion'] lazy load 제외할 dom의 class name. (ex swipe area)
     * @param {Function} [options.callback] callback 함수
     * @example
     * .lazy.renderAll({
     *     errorSrc: 'http://i.jobkorea.kr/content/images/m/....../error_image.png',
     *     callback: function (element, op) {
     *         console.log(element, 'has been loaded')
     *     }
     * });
     */
    lazy.renderAll = function(opts) {
        opts = opts || {};
        callback = opts.callback || callback;
        errorSrc = opts.errorSrc || errorSrc;
        exclusion = opts.exclusion || exclusion;

        var nodes = document.querySelectorAll('[data-original], [data-bg-original], .lazyBg');
        var length = nodes.length;
        var elem;
        for (var i = 0; i < length; i++) {
            elem = nodes[i];
            if (isExclusion(elem)) {
                imgRender(elem);
            }
        }
        lazy.detach();
    };

    /**
     * image lazy load render 함수.
     * @memberof mon.lazy
     * @function render
     * @param {Object} context context 값을 생략하면, scroll type lazy load 함수의 내부함수로 동작하고, context 값이 있으면 해당 영역의 모든 이미지를 불러온다.(ex swipe 영역 image lazy load시 사용)
     * @example
     * .lazy.render(swiper.container[0].querySelector('.swiper-slide-active')); // .swiper-slide-active 영역의 모든 이미지 로드
     */
    lazy.render = function(context) {
        var nodes = (context || document).querySelectorAll('[data-original], [data-bg-original], .lazyBg');
        var length = nodes.length;
        var elem;
        var i;
        if (context == null) {
            var view = {
                l: 0 - offset.l,
                t: 0 - offset.t,
                b: (root.innerHeight || document.documentElement.clientHeight) + offset.b,
                r: (root.innerWidth || document.documentElement.clientWidth) + offset.r
            };
            for (i = 0; i < length; i++) {
                elem = nodes[i];
                if ((inView(elem, view))&&(isExclusion(elem))) {
                    imgRender(elem);
                }
            }
        } else {
            for (i = 0; i < length; i++) {
                elem = nodes[i];
                imgRender(elem);
            }
        }

        if (!length) {
            lazy.detach();
        }
    };

    /**
     * image lazy load scroll event 제거 함수
     * @memberof mon.lazy
     * @function detach
     */
    lazy.detach = function() {
        if (document.removeEventListener) {
            root.removeEventListener('scroll', debounceOrThrottle);
        } else {
            root.detachEvent('onscroll', debounceOrThrottle);
        }
        clearTimeout(poll);
    };

    /**
     * JS file lazy load 함수
     * @memberof mon.lazy
     * @function js
     * @param {String} url js url
     * @param {Function} callback callback 함수
     * @example
     * .lazy.js("http://m.jobkorea.co.kr/include/js/swiper.min_3.3.1.js", function() {
     *     var swiperLazy = function(container) {
     *         core.lazy.render(container.querySelector('.swiper-slide-active'));
     *         core.lazy.render(container.querySelector('.swiper-slide-next'));
     *         core.lazy.render(container.querySelector('.swiper-slide-prev'));
     *     };
     *     var swiperEvent = new Swiper('#event .swiper-container', {
     *         loop: true,
     *         paginationType: "fraction",
     *         onSlideChangeStart : function(swiper) { // init도 실행됨
     *             swiperLazy(swiper.container[0]);
     *         }
     *     });
     * });
     */
    lazy.js = function(url, callback) {
        if (url == null) {
            return false;
        }
        callback = callback || function() {};
        var scriptEle = document.createElement('script');
        scriptEle.type = 'text/javascript';
        var loaded = false;
        scriptEle.onreadystatechange = function() {
            if (this.readyState == 'loaded' || this.readyState == 'complete') {
                if (loaded) return;
                loaded = true;
                callback();
            }
        };
        scriptEle.onload = function() {
            callback();
        };
        scriptEle.src = url;
        document.getElementsByTagName('head')[0].appendChild(scriptEle);
    };

    /**
     * JS file lazy load 함수(Promise type)
     * @memberof mon.lazy
     * @function jsPromise
     * @param {String} url js url
     * @param {Function} callback callback 함수
     */
    lazy.jsPromise = function(url, callback) {
        return new Promise(function(resolve, reject) {
            lazy.js(url, function() {
                callback();
                resolve(url+" loaded");
            });
        });
    };

    /**
     * youTube iframe lazy load 함수
     * @memberof mon.lazy
     * @function youTube
     * @param {String} [element='.lazyYoutube'] youTube iframe이 생성될 영역
     * @param {Object} options html data-* attribute options
     * @param {String} options.data-src (html data-* attribute) youTube 동영상 url
     * @param {String} [options.data-title] (html data-* attribute) iframe title
     * @param {Object} [options.data-options] (html data-* attribute) youTube 동영상 옵션값(width, height, allowfullscreen 등)
     */
    lazy.youTube = function(ele) {
        ele = ele || '.lazyYoutube';

        var nodes = document.querySelectorAll(ele),
            l = nodes.length, i, elem, youTubeIfr, options, title;

        for(i=0; i<l; i++) {
            elem = nodes[i];
            youTubeIfr = document.createElement('iframe');
            options = JSON.parse(elem.getAttribute('data-options')) || {};

            var srcValue=elem.getAttribute('data-src');
            if ((srcValue != null)&&(srcValue != "")) {
                title=elem.getAttribute('data-title') || '';
                youTubeIfr.setAttribute('src', elem.getAttribute('data-src'));
                youTubeIfr.setAttribute('title', title);
                youTubeIfr.setAttribute('frameborder', '0');
                youTubeIfr.setAttribute('allowfullscreen', '');

                for (var key in options) {
                    youTubeIfr.setAttribute(key, options[key]);
                }

                elem.appendChild(youTubeIfr);
                //console.log('ok:', srcValue);
                elem.removeAttribute('data-src');
                elem.removeAttribute('data-title');
                elem.removeAttribute('data-options');
            }
            // else {
            //  console.log('error');
            // }
        }
    };

    return lazy;
})();

/**
 * cookie
 * @namespace mon.cookie
 * @type {Object}
 * @property {Function} get 쿠키값 얻기
 * @property {Function} set 쿠키값 저장
 * @property {Function} setOnce 쿠키저장, 만료기간 셋팅 안함. 브라우저 종료시 삭제
 * @property {Function} del 쿠키 삭제
 */
core.cookie = {
    /**
     * 쿠키값 얻기
     * @memberof mon.cookie
     * @function get
     * @param {String} name name
     */
    get : function(name) {
        var nameStr = name + "=";
        var nameLen = nameStr.length;
        var cookieLen = document.cookie.length; //쿠키 값이 없을시 기본적으로 45이다.  document.cookie.length >= 45

        // a로 지정시 : document.cookie ==> lucya=a; ASPSESSIONIDQGQQGLDC=GKDDHCPDJBOBAONCMJLHBCCN
        var i = 0;
        while (i < cookieLen) {
            var j = i + nameLen;
            if (document.cookie.substring(i, j) == nameStr) {
                var end = document.cookie.indexOf(";", j); // ;의 위치
                if (end == -1) end = document.cookie.length;
                return unescape(document.cookie.substring(j, end)); // 쿠키값 반환
            }
            i = document.cookie.indexOf(" ", i) + 1;
            if (i == 0) {
                break;
            }
        }
    },

    /**
     * 쿠키값 저장
     * @memberof mon.cookie
     * @function set
     * @param {String} name name
     * @param {String} value value
     */
    set : function(name, value) {
        var expires = new Date();
        var path, domain, secure;

        var argv = arguments;
        var argc = arguments.length;
        if (argc > 2) {
            expires.setTime(expires.getTime() + (1000 * 60 * argv[2])); // argv[2]분동안 쿠키 유효
        } else {
            expires = null;
        }

        path = (argc > 3) ? argv[3] : null;
        domain = (argc > 4) ? argv[4] : null;
        secure = (argc > 5) ? argv[5] : false;

        document.cookie = name + "=" + escape(value) +
            ((expires == null) ? "" : (";expires=" + expires.toGMTString())) +
            ((path == null) ? "" : (";path=" + path)) +
            ((domain == null) ? "" : (";domain=" + domain)) +
            ((secure == true) ? " ;secure" : "");
    },

    /**
     * 쿠키저장, 만료기간 셋팅 안함. 브라우저 종료시 삭제
     * @memberof mon.cookie
     * @function setOnce
     * @param {String} name name
     * @param {String} value value
     */
    setOnce : function(name, value) {
        document.cookie = name + "=" + escape(value) + ";path=/;";
    },

    /**
     * 쿠키 삭제
     * @memberof mon.cookie
     * @function del
     * @param {String} name name
     */
    del : function(name) {
        var today = new Date();

        today.setTime(today.getTime() - 1);
        var value = this.get(name);
        if (value != null) {
            document.cookie = name + "=" + escape(value) + ("; expires=" + today.toGMTString()); // 쿠키삭제
            return (value + " 삭제완료");
        } else {
            return ("존재하지 않음");
        }
    }
};

if (typeof module !== 'undefined' && module.exports) {
    module.exports = core;
} else {
    Object.assign(window, core);
}
