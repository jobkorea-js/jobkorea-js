/**
 * Promise & Polyfill
 * @author Jobkorea Development Lab
 * @version 1.0.1
 *
 */
(function (root) {
    // Store setTimeout reference so promise-polyfill will be unaffected by
    // other code modifying setTimeout (like sinon.useFakeTimers())
    var setTimeoutFunc = setTimeout;

    function noop() {}

    // Polyfill for Function.prototype.bind
    function bind(fn, thisArg) {
        return function () {
            fn.apply(thisArg, arguments);
        };
    }

    /**
     * Promise 객체. 이 객체는 비동기 계산을 위해 사용됩니다. Promise는 아직은 아니지만 나중에 완료될 것으로 기대되는 연산을 표현합니다.
     * @class Promise
     * @param {function} fn - executor 함수. resolve 및 reject 인수를 통해 다른 함수에 전달될 함수입니다. executor 함수는 resolve 및 reject 함수를 제공하는 promise 구현에 의해 즉시 실행됩니다. (executor는 Promise 생성자가 객체를 반환하기도 전에 호출됩니다.) resolve 및 reject 함수는 프로미스에 바인딩되고 둘을 호출하면 각각 프로미스를 이행(fulfill) 또는 거부(reject, 거절)합니다. executor는 일부 비동기 작업(work)을 시작하리라 예상되고 작업이 완료되면 프로미스의 최종값을 결정(resolve, 확정)하거나 오류가 발생하면 거부하기 위해 resolve 또는 reject 함수를 호출합니다.
     * @example
     * new Promise( function(resolve, reject) { ... } );
     *
     * 'use strict';
     *  var promiseCount = 0;

     *  function testPromise() {
     *      var thisPromiseCount = ++promiseCount;
     *      var log = document.getElementById('log');
     *      log.insertAdjacentHTML('beforeend', thisPromiseCount +
     *          ') Started (<small>Sync code started</small>)<br/>');

     *      // 새 약속을 (생성)합니다: 이 프로미스의 숫자 셈을 약속합니다, 1부터 시작하는(3s 기다린 뒤)
     *      var p1 = new Promise(
     *          // resolver(결정자) 함수는 프로미스 (이행을) 결정 또는 거부하는
     *          // 능력과 함께 호출됩니다
     *          function(resolve, reject) {
     *              log.insertAdjacentHTML('beforeend', thisPromiseCount +
     *                  ') Promise started (<small>Async code started</small>)<br/>');
     *              // 이는 비동기를 만드는 예에 불과합니다
     *              window.setTimeout(
     *                  function() {
     *                      // 프로미스 이행 !
     *                      resolve(thisPromiseCount);
     *                  }, Math.random() * 2000 + 1000);
     *          }
     *      );

     *      // 프로미스가 then() 호출로 결정된/이행된 경우 무엇을 할 지를 정의하고,
     *      // catch() 메서드는 프로미스가 거부된 경우 무엇을 할 지를 정의합니다.
     *      p1.then(
     *          // 이행값 기록
     *          function(val) {
     *              log.insertAdjacentHTML('beforeend', val +
     *                  ') Promise fulfilled (<small>Async code terminated</small>)<br/>');
     *          })
     *      .catch(
     *          // 거부 이유 기록
     *          function(reason) {
     *              console.log('Handle rejected promise ('+reason+') here.');
     *          });

     *      log.insertAdjacentHTML('beforeend', thisPromiseCount +
     *          ') Promise made (<small>Sync code terminated</small>)<br/>');
     *  }
     */
    function Promise(fn) {
        if (typeof this !== 'object') throw new TypeError('Promises must be constructed via new');
        if (typeof fn !== 'function') throw new TypeError('not a function');
        this._state = 0;
        this._handled = false;
        this._value = undefined;
        this._deferreds = [];

        doResolve(fn, this);
    }

    function handle(self, deferred) {
        while (self._state === 3) {
            self = self._value;
        }
        if (self._state === 0) {
            self._deferreds.push(deferred);
            return;
        }
        self._handled = true;
        Promise._immediateFn(function () {
            var cb = self._state === 1 ? deferred.onFulfilled : deferred.onRejected;
            if (cb === null) {
                (self._state === 1 ? resolve : reject)(deferred.promise, self._value);
                return;
            }
            var ret;
            try {
                ret = cb(self._value);
            } catch (e) {
                reject(deferred.promise, e);
                return;
            }
            resolve(deferred.promise, ret);
        });
    }

    function resolve(self, newValue) {
        try {
            // Promise Resolution Procedure: https://github.com/promises-aplus/promises-spec#the-promise-resolution-procedure
            if (newValue === self) throw new TypeError('A promise cannot be resolved with itself.');
            if (newValue && (typeof newValue === 'object' || typeof newValue === 'function')) {
                var then = newValue.then;
                if (newValue instanceof Promise) {
                    self._state = 3;
                    self._value = newValue;
                    finale(self);
                    return;
                } else if (typeof then === 'function') {
                    doResolve(bind(then, newValue), self);
                    return;
                }
            }
            self._state = 1;
            self._value = newValue;
            finale(self);
        } catch (e) {
            reject(self, e);
        }
    }

    function reject(self, newValue) {
        self._state = 2;
        self._value = newValue;
        finale(self);
    }

    function finale(self) {
        if (self._state === 2 && self._deferreds.length === 0) {
            Promise._immediateFn(function() {
                if (!self._handled) {
                    Promise._unhandledRejectionFn(self._value);
                }
            });
        }

        for (var i = 0, len = self._deferreds.length; i < len; i++) {
            handle(self, self._deferreds[i]);
        }
        self._deferreds = null;
    }

    function Handler(onFulfilled, onRejected, promise) {
        this.onFulfilled = typeof onFulfilled === 'function' ? onFulfilled : null;
        this.onRejected = typeof onRejected === 'function' ? onRejected : null;
        this.promise = promise;
    }

    /**
     * Take a potentially misbehaving resolver function and make sure
     * onFulfilled and onRejected are only called once.
     *
     * Makes no guarantees about asynchrony.
     */
    function doResolve(fn, self) {
        var done = false;
        try {
            fn(function (value) {
                if (done) return;
                done = true;
                resolve(self, value);
            }, function (reason) {
                if (done) return;
                done = true;
                reject(self, reason);
            });
        } catch (ex) {
            if (done) return;
            done = true;
            reject(self, ex);
        }
    }

    Promise.prototype['catch'] = function (onRejected) {
        return this.then(null, onRejected);
    };

    Promise.prototype.then = function (onFulfilled, onRejected) {
        var prom = new (this.constructor)(noop);

        handle(this, new Handler(onFulfilled, onRejected, prom));
        return prom;
    };

    Promise.all = function (arr) {
        var args = Array.prototype.slice.call(arr);

        return new Promise(function (resolve, reject) {
            if (args.length === 0) return resolve([]);
            var remaining = args.length;

            function res(i, val) {
                try {
                    if (val && (typeof val === 'object' || typeof val === 'function')) {
                        var then = val.then;
                        if (typeof then === 'function') {
                            then.call(val, function (val) {
                                res(i, val);
                            }, reject);
                            return;
                        }
                    }
                    args[i] = val;
                    if (--remaining === 0) {
                        resolve(args);
                    }
                } catch (ex) {
                    reject(ex);
                }
            }

            for (var i = 0; i < args.length; i++) {
                res(i, args[i]);
            }
        });
    };

    Promise.resolve = function (value) {
        if (value && typeof value === 'object' && value.constructor === Promise) {
            return value;
        }

        return new Promise(function (resolve) {
            resolve(value);
        });
    };

    Promise.reject = function (value) {
        return new Promise(function (resolve, reject) {
            reject(value);
        });
    };

    Promise.race = function (values) {
        return new Promise(function (resolve, reject) {
            for (var i = 0, len = values.length; i < len; i++) {
                values[i].then(resolve, reject);
            }
        });
    };

    // Use polyfill for setImmediate for performance gains
    Promise._immediateFn = (typeof setImmediate === 'function' && function(fn) {
        setImmediate(fn);
    }) ||
    function (fn) {
        setTimeoutFunc(fn, 0);
    };

    Promise._unhandledRejectionFn = function _unhandledRejectionFn(err) {
        if (typeof console !== 'undefined' && console) {
            console.warn('Possible Unhandled Promise Rejection:', err); // eslint-disable-line no-console
        }
    };

    /**
     * Set the immediate function to execute callbacks
     * @param fn {function} Function to execute
     * @deprecated
     */
    Promise._setImmediateFn = function _setImmediateFn(fn) {
        Promise._immediateFn = fn;
    };

    /**
     * Change the function to execute on unhandled rejection
     * @param {function} fn Function to execute on unhandled rejection
     * @deprecated
     */
    Promise._setUnhandledRejectionFn = function _setUnhandledRejectionFn(fn) {
        Promise._unhandledRejectionFn = fn;
    };

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = { Promise: Promise};
    } else if (!root.Promise) {
        root.Promise = Promise;
    }
})(this);

// [ES6] Object.assign() Polyfill - https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
if (typeof Object.assign != 'function') {
    // console.log("Object.assign Polyfill use");
    Object.assign = function(target, varArgs) {
        if (target == null) { // TypeError if undefined or null
            throw new TypeError('Cannot convert undefined or null to object');
        }

        var to = Object(target);

        for (var index = 1; index < arguments.length; index++) {
            var nextSource = arguments[index];

            if (nextSource != null) { // Skip over if undefined or null
                for (var nextKey in nextSource) {
                    // Avoid bugs when hasOwnProperty is shadowed
                    if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                        to[nextKey] = nextSource[nextKey];
                    }
                }
            }
        }
        return to;
    };
}
