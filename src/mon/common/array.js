/**
 * @author Jobkorea Development Lab
 * @desc 알바몬 array 관련 모듈
 * @dependency core.js
 * @namespace mon.array
 */
var array = (function () {

    'use strict';

    var type = require('./type');

    /**
     * 배열객체만큼 콜백함수에 담아 실행해준다.(jquery each와 동일)
     * @memberof mon.array
     * @param {object} obj - 대상객체 
     * @param {function} callback - 콜백함수
     * @returns {object} 콜백객체
     */
    var each = function (obj, callback) {
        var length, i = 0;
        if (type.isArray(obj)) {
            length = obj.length;
            for (; i < length; i++) {
                if (callback.call(obj[i], i, obj[i]) === false) {
                    break;
                }
            }
        } else {
            for (i in obj) {
                if (callback.call(obj[i], i, obj[i]) === false) {
                    break;
                }
            }
        }
        return obj;
    };

    return {
        each: each
    };
})();

if (typeof module !== 'undefined' && module.exports) {
    module.exports = array;
} else if (typeof mon !== 'undefined') {
    if (typeof Object.assign !== 'undefined') {
        Object.assign(mon, { array: array });
    } else {
        var extend = function() {
            var extended = {},
                key, prop;
            for(key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        extend(mon, { array: array });
    }
} else {
    var mon = mon || {};
    mon.array = array;
}