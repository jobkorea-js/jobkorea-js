/**
 * @author Jobkorea Development Lab
 * @desc 알바몬 데이터 타입 체크 모듈
 * @dependency core.js
 * @namespace mon.type
 */
var type = (function () {
    'use strict';

    /**
     * 대상 데이터가 null 인지 검사한다.
     * 대상 데이터가 null 이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isNull
     * @param {*} obj 검사 대상 
     * @returns {boolean} null 여부
     * @example
     * mon.type.isNull(null);    // true
     * mon.type.isNull(1);       // false
     */
    var isNull = function(obj) {
        return obj === null;
    };
    /**
     * 대상 데이터가 undefined 인지 검사한다.
     * 대상 데이터가 undefined 이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isUndefined
     * @param {*} obj 검사 대상
     * @returns {boolean} undefined 여부
     * @example
     * mon.type.isUndefined(undefined);    // true
     * mon.type.isUndefined('undefined');  // false
     */
    var isUndefined = function(obj) {
        return typeof obj === 'undefined';
    };
    /**
     * 대상 데이터가 객체인지 검사한다.
     * 대상 데이터가 객체이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isObject
     * @param {*} obj 검사 대상
     * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
     * @returns {boolean} 객체 여부
     * @example
     * mon.type.isObject(new Object());   // true - new object
     * mon.type.isObject({});             // true - new object
     * mon.type.isObject('');             // false - new primitive string
     * mon.type.isObject(0);              // false - new primitive number
     * mon.type.isObject(false);          // false - new primitive boolean
     * mon.type.isObject([]);             // true - new array object
     * mon.type.isObject(function() {});  // true - new function object
     * mon.type.isObject(/()/);           // true - new regexp object
     * mon.type.isObject(null);           // false
     */
    var isObject = function(obj, isSafe) {
        if (isSafe) {
            return Object.prototype.toString.call(obj) === '[object Object]';
        }
        return obj === Object(obj);
    };
    /**
     * 대상 데이터가 배열 객체인지 검사한다.
     * 대상 데이터가 배열 이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isArray
     * @param {*} obj 검사 대상
     * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
     * @returns {boolean} 배열 여부
     * @example
     * mon.type.isArray(new Array());  // true
     * mon.type.isArray([]);           // true
     * mon.type.isArray(1);            // false
     */
    var isArray = function(obj, isSafe) {
        if (isSafe) {
            return Object.prototype.toString.call(obj) === '[object Array]';
        }
        return obj instanceof Array;
    };
    /**
     * 대상 데이터가 함수인지 검사한다.
     * 대상 데이터가 함수이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isFunction
     * @param {*} obj 검사 대상
     * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
     * @returns {boolean} 함수 여부
     * @example
     * mon.type.isFunction(new Function());  // true
     * mon.type.isFunction(function() {});   // true
     * mon.type.isFunction('function() {}'); // false
     */
    var isFunction = function(obj, isSafe) {
        if (isSafe) {
            return Object.prototype.toString.call(obj) === '[object Function]';
        }
        return typeof obj === 'function' || obj instanceof Function;
    };
    /**
     * 대상 데이터가 숫자인지 검사한다.
     * 대상 데이터가 숫자이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isNumber
     * @param {*} obj 검사 대상
     * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
     * @returns {boolean} 숫자 여부
     * @example
     * mon.type.isNumber(new Number());  // true
     * mon.type.isNumber(1);             // true
     * mon.type.isNumber('1');           // false
     */
    var isNumber = function(obj, isSafe) {
        if (isSafe) {
            return Object.prototype.toString.call(obj) === '[object Number]';
        }
        return typeof obj === 'number' || obj instanceof Number;
    };
    /**
     * 대상 데이터가 문자인지 검사한다.
     * 대상 데이터가 문자이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isString
     * @param {*} obj 검사 대상 
     * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
     * @returns {boolean} 문자 여부
     * @example
     * mon.type.isString(new String()); // true
     * mon.type.isString('1');          // true
     * mon.type.isString(1);            // false
     */
    var isString = function(obj, isSafe) {
        if (isSafe) {
            return Object.prototype.toString.call(obj) === '[object String]';
        }
        return typeof obj === 'string' || obj instanceof String;
    };
    /**
     * 대상 데이터가 Boolean인지 검사한다.
     * 대상 데이터가 Boolean이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isBoolean
     * @param {*} obj 검사 대상 
     * @param {boolean} [isSafe=false] 안전검사 여부 (frame/iframe/팝업 등 다른 window 환경일 경우 사용)
     * @returns {boolean} Boolean 여부
     * @example
     * mon.type.isBoolean(new Boolean()); // true
     * mon.type.isBoolean(true);          // true
     * mon.type.isBoolean(false);         // true
     * mon.type.isBoolean(0);             // false
     * mon.type.isBoolean(1);             // false
     * mon.type.isBoolean('true');        // false
     */
    var isBoolean = function(obj, isSafe) {
        if (isSafe) {
            return Object.prototype.toString.call(obj) === '[object Boolean]';
        }
        return typeof obj === 'boolean' || obj instanceof Boolean;
    };
    /**
     * 대상 데이터가 빈문자열인지 검사한다.
     * 대상 데이터가 빈문자열이면 true, 아니면 false를 반환한다.
     * @param {*} obj 검사 대상
     * @returns {boolean} 빈문자열 여부
     * @ignore
     */
    var _isEmptyString = function(obj) {
        return isString(obj) && obj === '';
    };
    /**
     * 대상 데이터가 agruments 객체인지 검사한다.
     * 대상 데이터가 agruments 이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isArguments
     * @param {*} obj 검사 대상
     * @returns {boolean} agruments 여부
     * @example
     * var fn = function(param1) {
     *      mon.type.isArguments(param1);               // false
     *      mon.type.isArguments(this.fn.arguments);    // true
     * };
     * fn(1, 2, 3);
     */
    var isArguments = function(obj) {
        return isExist(obj) && ((Object.prototype.toString.call(obj) === '[object Arguments]') || !!obj.callee);
    };
    /**
     * 대상 데이터의 속성이 존재하는지 검사한다.
     * 대상 데이터의 속성이 존재하면 true, 아니면 false를 반환한다.
     * @param {*} obj 검사 대상
     * @returns {boolean} 속성 존재 여부
     * @ignore
     */
    var _hasOwnProperty = function(obj) {
        var key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                return true;
            }
        }
        return false;
    };
    /**
     * 대상 데이터가 존재하는지 검사한다.
     * 대상 데이터가 존재하면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isExist
     * @param {*} obj 검사 대상
     * @returns {boolean} 존재 여부
     * @example
     * mon.type.isExist('');        // true
     * mon.type.isExist(0);         // true
     * mon.type.isExist([]);        // true
     * mon.type.isExist({});        // true
     * mon.type.isExist(null);      // false
     * mon.type.isExist(undefined); // false
     */
    var isExist = function(obj) {
        return !isUndefined(obj) && !isNull(obj);
    };
    /**
     * 대상 데이터가 빈값인지 검사한다.
     * 대상 데이터가 빈값이면 true, 아니면 false를 반환한다.
     * @memberof mon.type
     * @method isEmpty
     * @param {*} obj 검사 대상
     * @returns {boolean} 반값 여부
     * @example
     * mon.type.isEmpty('');        // true
     * mon.type.isEmpty(0);         // false
     * mon.type.isEmpty([]);        // true
     * mon.type.isEmpty({});        // true
     * mon.type.isEmpty(null);      // true
     * mon.type.isEmpty(undefined); // true
     */
    var isEmpty = function(obj) {
        // 값존재 검사
        if (!isExist(obj) || _isEmptyString(obj)) {
            return true;
        }
        // 배열 검사
        if (isArray(obj) || isArguments(obj)) {
            return obj.length === 0;
        }
        // 속성 검사
        if (isObject(obj) && !isFunction(obj)) {
            return !_hasOwnProperty(obj);
        }
        return false;
    };
    
    return {
        isNull: isNull,
        isUndefined: isUndefined,
        isObject: isObject,
        isArray: isArray,
        isFunction: isFunction,
        isNumber: isNumber,
        isString: isString,
        isBoolean: isBoolean,
        isArguments: isArguments,
        isExist: isExist,
        isEmpty: isEmpty
    };
})();

if (typeof module !== 'undefined' && module.exports) {
    module.exports = type;
} else if (typeof mon !== 'undefined') {
    if (typeof Object.assign !== 'undefined') {
        Object.assign(mon, { type: type });
    } else {
        var extend = function() {
            var extended = {},
                key, prop;
            for(key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        extend(mon, { type: type });
    }
} else {
    var mon = mon || {};
    mon.type = type;
}