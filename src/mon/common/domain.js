/**
 * @author Jobkorea Development Lab
 * @desc 알바몬 사이트 domain 관련 모듈
 * @dependency core.js, mon.js
 * @namespace mon.domain
 */
var domain = (function(domain) {
    'use strict';

    var type = require('./type');
    
    /** 도메인 관련 옵션 */
    domain.options = {};
    
    // 서버 위치별 정규식 정의
    var regEx = {
        dev: /^(dev)\-?(www)?(.albamon.com)$/i,
        test: /^(test)\-?(www)?(.albamon.com)$/i,
        stg: /^(www19)(.albamon.com)$/i,
        live: /^(www)(.albamon.com)$/i
    };
    var regExM = {
        dev: /^(dev)\-?(m)?(.albamon.com)$/i,
        test: /^(test)\-?(m)?(.albamon.com)$/i,
        stg: /^(m5)(.albamon.com)$/i,
        live: /^(m)(.albamon.com)$/i
    };

    /**
     * 현재 도메인이 데스크탑인지 검사한다.
     * @memberof mon.domain
     * @method isPc
     * @returns {boolean} 검사 결과 여부
     * @example
     * mon.domain.isPc();      // (www.domain.com 에서) true
     * mon.domain.isPc();      // (m.domain.com 에서) false
     */
    var isPc = function() {
        var result = false;
        for (var key in regEx) {
            if (regEx[key].test(document.domain.toLowerCase())) {
                result = true ;
            }
        }
        return result;
    };

    /**
     * 현재 도메인이 모바일사이트인지 검사한다.
     * @memberof mon.domain
     * @method isMobile
     * @returns {boolean} 검사 결과 여부
     * @example
     * mon.domain.isMobile();      // (www.domain.com 에서) false
     * mon.domain.isMobile();      // (m.domain.com 에서) true
     */
    var isMobile = function() {
        var result = false;
        for (var key in regExM) {
            if (regExM[key].test(document.domain.toLowerCase())) {
                result = true;
            }
        }
        return result;
    };

    /**
     * 현재 도메인의 서버위치를 가져온다.<br>
     * - dev: 개발서버(로컬)<br>
     * - test: 테스트서버<br>
     * - stg: 스테이징서버<br>
     * - live: 실서버
     * @memberof mon.domain
     * @method getLocation
     * @returns {string} 현재 서버위치
     * @example
     * mon.domain.getLocation();      // (www.domain.com 에서) "live"
     * mon.domain.getLocation();      // (test-www.domain.com 에서) "test"
     */
    var getLocation = function() {
        if (isPc()) {
            for (var key in regEx) {
                if (regEx[key].test(document.domain.toLowerCase())) {
                    return key;
                }
            }
        } else if (isMobile()) {
            for (var key in regExM) {
                if (regExM[key].test(document.domain.toLowerCase())) {
                    return key;
                }
            }
        }
    };

    /**
     * 현재 도메인이 개발사이트인지 검사한다.
     * @memberof mon.domain
     * @method isDev
     * @returns {boolean} 검사 결과 여부
     * @example
     * mon.domain.isDev();      // (www.domain.com 에서) false
     * mon.domain.isDev();      // (dev-www.domain.com 에서) true
     * mon.domain.isDev();      // (m.domain.com 에서) false
     * mon.domain.isDev();      // (dev-m.domain.com 에서) true
     */
    var isDev = function() {
        if (isPc()) {
            if (regEx['dev'].test(document.domain.toLowerCase())) {
                return true;
            }
        } else if (isMobile()) {
            if (regExM['dev'].test(document.domain.toLowerCase())) {
                return true;
            }
        }
        return false;
    };

    /**
     * host명 가져오기
     * @param {string} location 서버위치
     * @ignore
     */
    var _getHost = function (location) {
        var host = '',
            device = isPc() ? 'www' : 'm';

        location = type.isEmpty(location) ? getLocation() : location;

        switch (location) {
            case 'dev':
                host = device === 'm' ? 'dev-m' : 'dev-www';
                break;
            case 'test':
                host = device === 'm' ? 'test-m' : 'test-www';
                break;
            case 'stg':
                host = device === 'm' ? 'm5' : 'www19';
            default:
                host = device === 'm' ? 'm' : 'www';
                break;
        }
        return host;
    };

    /**
     * 현재 도메인의 프로토콜정보를 가져온다. (document.location.protocol와 동일)
     * @memberof mon.domain
     * @method getProtocol
     * @returns {string} 프로토콜 정보
     * @example
     * mon.domain.getProtocol();      // (http://www.domain.com 일 경우) "http:"
     * mon.domain.getProtocol();      // (https://www.domain.com 일 경우) "https:"
     */
    var getProtocol = function () {
        return document.location.protocol;
    };
    
    /**
     * 현재 도메인의 포트정보를 가져온다. (document.location.port와 동일)
     * @memberof mon.domain
     * @method getPort
     * @returns {string} 포트 정보
     * @example
     * mon.domain.getPort();      // (http://www.domain.com 일 경우) ""
     * mon.domain.getPort();      // (https://www.domain.com:8080 일 경우) "8080"
     */
    var getPort = function() {
        return document.location.port;
    };

    /**
     * set options
     * @private
     */
    var _setOption = function(opts) {
        // location
        opts.location = type.isEmpty(opts.location) ? getLocation() : opts.location;
        // device
        opts.device = type.isEmpty(opts.device) ? isPc() ? 'pc' : 'm' : opts.device;
        // protocol
        opts.protocol = type.isEmpty(opts.protocol) ? getProtocol() : opts.protocol;
        // port
        opts.port = type.isEmpty(opts.port) ? document.location.port : opts.port;
        // host
        opts.host = type.isEmpty(opts.host) ? _getHost(opts.location) : opts.host;
        // group
        opts.domainName = type.isEmpty(opts.domainName) ? 'albamon.com' : opts.domainName;

        return opts;
    };
    // init options
    _setOption(domain.options);

    /**
     * 특정 사이트의 도메인 정보를 가져온다.
     * @memberof mon.domain
     * @method getDomain
     * @param {object} opts - 옵션값. 없을 경우 현재 도메인을 기준으로 처리한다.
     * @param {string} [opts.device] - 디바이스정보
     * @param {string} [opts.location] - 서버위치정보 (dev: 개발서버, test: 테스트서버, stg: 스테이징서버, live: 실서버)
     * @param {string} [opts.port] - 포트정보
     * @param {boolean} [opts.isFull=false] - 전체 도메인주소 여부(true: http://www.albamon.com, false: www.albamon.com)
     * @param {string} [opts.domainName] - 도메인명 (file1.jobkorea.co.kr, file2.jobkorea.co.kr, fileco.jobkorea.co.kr, file.albamon.com, 그외는 모두 albamon.com으로 처리)
     * @returns {string} - 도메인 정보
     * @example
     * mon.domain.getDomain();                                 // (www.albamon.com 사이트에서) "www.albamon.com"
     * mon.domain.getDomain();                                 // (test-www.albamon.com 사이트에서) "test-albamon.com"
     * mon.domain.getDomain({device: 'm'});                    // (m.albamon.com 사이트에서) "m.albamon.com"
     * mon.domain.getDomain({device: 'm'});                    // (test-m.albamon.com 사이트에서) "test-m.albamon.com"
     * mon.domain.getDomain({device: 'm', location: 'stg'});   // (www.albamon.com 사이트에서) "m5.albamon.com"
     * mon.domain.getDomain({device: 'm', location: 'stg', isFull: true});   // (www.albamon.com 사이트에서) "http://m5.albamon.com"
     */
    var getDomain = function(opts) {
        var result = '',
            opts = type.isEmpty(opts) ? domain.options : _setOption(opts);

        if (opts.domainName === 'file1.jobkorea.co.kr') {
            if (opts.location === 'dev' || opts.location === 'test') {
                result = 'test-' + opts.domainName;
            } else {
                result = opts.domainName;
            }
        } else if (opts.domainName === 'file2.jobkorea.co.kr') {
            if (opts.location === 'dev' || opts.location === 'test') {
                result = 'test-' + opts.domainName;
            } else {
                result = opts.domainName;
            }
        } else if (opts.domainName ==='fileco.jobkorea.co.kr') {
            if (opts.location === 'dev' || opts.location === 'test') {
                result = 'test-' + opts.domainName;
            } else {
                result = opts.domainName;
            }
        } else if (opts.domainName ==='albamon.com') {
            if (opts.location === 'dev' || opts.location === 'test') {
                result = 'test-' + opts.domainName;
            } else {
                result = opts.domainName;
            }
        } else {
            if (opts.port.toString() !== '80' && opts.port.toString() !== '') {
                result = opts.host + '.albamon.com:' + opts.port;
            } else {
                result = opts.host + '.albamon.com' + opts.port;
            }
        }

        if (opts.isFull) {
            result = opts.protocol + '//' + result;
        }

        return result;
    };

    /**
     * 잡코리아 파일 서버의 도메인 정보를 가져온다.
     * @memberof mon.domain
     * @method getDomainJKFile
     * @param {object} opts - 옵션값. getDomain()과 동일
     * @returns {string} - 도메인 정보
     * @example
     * mon.domain.getDomainJKFile();                        // (www.albamon.com 사이트에서) "file1.jobkorea.co.kr"
     * mon.domain.getDomainJKFile();                        // (test-www.albamon.com 사이트에서) "test-file1.jobkorea.co.kr"
     * mon.domain.getDomainJKFile({isFull: true});          // (www.albamon.com 사이트에서) "http://file1.jobkorea.co.kr"
     */
    var getDomainJKFile = function (opts) {
        opts = type.isEmpty(opts) ? domain.options : _setOption(opts);
        opts.domainName = 'file1.jobkorea.co.kr';
        return getDomain(opts);
    };

    /**
     * 잡코리아 파일2 서버의 도메인 정보를 가져온다.
     * @memberof mon.domain
     * @method getDomainJKFile2
     * @param {object} opts - 옵션값. getDomain()과 동일
     * @returns {string} - 도메인 정보
     * @example
     * mon.domain.getDomainJKFile2();                        // (www.albamon.com 사이트에서) "file2.jobkorea.co.kr"
     * mon.domain.getDomainJKFile2();                        // (test-www.albamon.com 사이트에서) "test-file2.jobkorea.co.kr"
     * mon.domain.getDomainJKFile2({isFull: true});          // (www.albamon.com 사이트에서) "http://file2.jobkorea.co.kr"
     */
    var getDomainJKFile2 = function (opts) {
        opts = type.isEmpty(opts) ? domain.options : _setOption(opts);
        opts.domainName = 'file2.jobkorea.co.kr';
        return getDomain(opts);
    };

    /**
     * 잡코리아 기업용 파일 서버의 도메인 정보를 가져온다.
     * @memberof mon.domain
     * @method getDomainJKFileCo
     * @param {object} opts - 옵션값. getDomain()과 동일
     * @returns {string} - 도메인 정보
     * @example
     * mon.domain.getDomainJKFileCo();                        // (www.albamon.com 사이트에서) "fileco.jobkorea.co.kr"
     * mon.domain.getDomainJKFileCo();                        // (test-www.albamon.com 사이트에서) "test-fileco.jobkorea.co.kr"
     * mon.domain.getDomainJKFileCo({isFull: true});          // (www.albamon.com 사이트에서) "http://fileco.jobkorea.co.kr"
     */
    var getDomainJKFileCo = function (opts) {
        opts = type.isEmpty(opts) ? domain.options : _setOption(opts);
        opts.domainName = 'fileco.jobkorea.co.kr';
        return getDomain(opts);
    };

    /**
     * 알바몬 파일 서버의 도메인 정보를 가져온다.
     * @memberof mon.domain
     * @method getDomainMoniFile
     * @param {object} opts - 옵션값. getDomain()과 동일
     * @returns {string} - 도메인 정보
     * @example
     * mon.domain.getDomainMoniFile();                        // (www.albamon.com 사이트에서) "file.albamon.com"
     * mon.domain.getDomainMoniFile();                        // (test-www.albamon.com 사이트에서) "test-file.albamon.com"
     * mon.domain.getDomainMoniFile({isFull: true});          // (www.albamon.com 사이트에서) "http://file.albamon.com"
     */
    var getDomainMoniFile = function (opts) {
        opts = type.isEmpty(opts) ? domain.options : _setOption(opts);
        opts.domainName = 'file.albamon.com';
        return getDomain(opts);
    };

    return {
        isPc: isPc,
        isMobile: isMobile,
        isDev: isDev,
        getLocation: getLocation,
        getProtocol: getProtocol,
        getPort: getPort,
        getDomain: getDomain,
        getDomainJKFile: getDomainJKFile,
        getDomainJKFile2: getDomainJKFile2,
        getDomainJKFileCo: getDomainJKFileCo,
        getDomainMoniFile: getDomainMoniFile
    };
})(this);

if (typeof module !== 'undefined' && module.exports) {
    module.exports = domain;
} else if (typeof mon !== 'undefined') {
    if (typeof Object.assign !== 'undefined') {
        Object.assign(mon, { domain: domain });
    } else {
        var extend = function() {
            var extended = {},
                key, prop;
            for(key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        extend(mon, { domain: domain });
    }
} else {
    var mon = mon || {};
    mon.domain = domain;
}