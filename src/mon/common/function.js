/**
 * @author Jobkorea Development Lab
 * @desc 알바몬 function 관련 모듈
 * @dependency core.js
 * @namespace mon.func
 */
var func = (function () {
    
    'use strict';

    var type = require('./type');

    /**
     * 대상 객체의 함수를 바인딩 한다.
     * @memberof mon.func
     * @method bind
     * @param {function} fn - 바인딩할 함수
     * @param {object} target - 대상객체
     * @example
     * test
     */
    var bind = function (fn, target) {
        var slice = Array.prototype.slice,
            args;
        if (type.isFunction(fn) === false) {
            throw new TypeError('target is not a function');
        }
        if (fn.bind) {
            return fn.bind.apply(fn, slice.call(arguments, 1));
        }
        args = slice.call(arguments, 2);
        return function() {
            return fn.apply(target, args.length ? args.concat(slice.call(arguments)) : arguments);
        };
    };  

    return {
        bind: bind
    };
})();

if (typeof module !== 'undefined' && module.exports) {
    module.exports = func;
} else if (typeof mon !== 'undefined') {
    if (typeof Object.assign !== 'undefined') {
        Object.assign(mon, { func: func });
    } else {
        var extend = function() {
            var extended = {},
                key, prop;
            for(key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        extend(mon, { func: func });
    }
} else {
    var mon = mon || {};
    mon.func = func;
}