/**
 * 유효성 검사 모듈
 * @author Jobkorea Development Lab
 * @dependency core.js, type.js
 * @namespace mon.validator
 * @example
 * // 규칙정의
 * var rules = {
 *     title: '글제목',    // 메시지에 표시할 타이틀
 *     required: true,     // 필수여부 규칙
 *     max: { max: 10, message: '최대 10자를 넘었습니다.' },   // 최대값 검사 규칙
 *     min: { min: 5 }                                         // 최소값 검사 규칙
 * };
 * // 검사처리
 * mon.validator.go('', rules);
 * // Result { isValid: false, errors: ["글제목이(가) 필요합니다."], each: function }
 * mon.validator.go('안녕!', rules);
 * // Result { isValid: false, errors: ["글제목은(는) 최소5자 이상이어야 합니다"], each: function }
 * mon.validator.go('안녕하세요. 정말 좋은 아침입니다.', rules);   
 * // Result { isValid: false, errors: ["최대 10자를 넘었습니다."], each: function }
 * mon.validator.go('안녕하세요.', rules);                         
 * // Result { isValid: true, errors: [], each: function }  
 */
var validator = (function() {
    'use strict';

    var type = require('./type');

    /**
     * {@link mon.validator.go} 유효성검사 실행 결과 객체.<br>
     * 이 객체는 {@link mon.validator.go} 함수를 통해 사용되며, 검사 결과를 해당 객체로 받는다.
     * @memberof mon.validator
     * @typedef {object} result
     * @alias result
     * @property {boolean} isValid - 유효성 검사 실패 여부
     * @property {Array.<string>} errors - 유효성 실패 알림문구 목록
     * @property {function} each - 유효성검사 실패 알림문구를 호출하는 콜백 함수
     * @example
     * var rule = {
     *     title: '홈페이지 주소',
     *     url: { message: '올바른 URL 주소형식이 아닙니다.' },
     *     email: true,
     *     required: true,
     *     max: 5
     * };
     * var result = mon.validator.go('http://www.domain.com', rule);
     * // Result { isValid: true, errors: [], each: function }
     * rule.stop = false;   // 연속검사 설정
     * var result = mon.validator.go('domain', rule);
     * result.each(function (e) {
     *     console.log(e);
     * });
     * // 홈페이지 주소은(는) 최대5자 이하이어야 합니다.
     * // 홈페이지 주소은(는) 메일주소 형식이어야 합니다.
     * // 올바른 URL 주소형식이 아닙니다.
     */
    var Result = function() {
        this.isValid = true;
        this.errors = [];
        this.each = function (callback) {
            var isFunc = callback && callback.constructor && callback.call && callback.apply,
                i = this.errors.length;
            while (i--) {
                if (isFunc) {
                    callback(this.errors[i]);
                }
            }
        };
    };

    /**
     * 유효성 검사 시작
     * @ignore
     */
    var _start = function(value, _rules) {
        var result = new Result(),
            title = '',
            stop = true,
            ignoreNull = false;
        // title 속성이 존재하면 담기
        if (_rules.hasOwnProperty('title')) {
            title = _rules.title;
        }
        // ignoreNull 속성이 존재하면 담기
        if (_rules.hasOwnProperty('ignoreNull')) {
            ignoreNull = _rules.ignoreNull;
        }
        // stop 속성이 존재하면 담기
        if (_rules.hasOwnProperty('stop')) {
            stop = _rules.stop;
        }
        for (var rule in _rules) {
            // 각 유효성검사에 실패했다면 추가검사 중지
            if (stop && !result.isValid) {
                break;
            }
            if (rules.hasOwnProperty(rule) && _isRule(rule)) {
                // rule에 정의된 항목 값
                var ruleValue = _rules[rule];
                // rules 항목에 정의된 rule인지 확인
                if (rules.hasOwnProperty(rule)) {
                    // 검사용 매개변수 생성
                    var params = {
                        ruleValue: ruleValue,
                        ruleName: rule,
                        title: title,
                        rule: rules[rule],
                        value: value,
                        ignoreNull: ignoreNull
                    };
                    // 유효성 검사 검사
                    _checkRule(params, result);
                } else {
                    throw rule + ' 규칙이 정의되지 않았습니다.';
                }
            }
        }

        return result;
    };
    /**
     * 규칙에 대한 유효성검사를 수행하고 오류를 포함한 결과를 반환
     * @ignore
     */
    var _checkRule = function(params, result) {
        // null을 무시하는 경우
        if (params.hasOwnProperty('ignoreNull')) {
            if (params.value === null && params.ignoreNull) {
                return;
            }
        }
        // 검사용 매개변수를 객체로 변환
        var _args = _getArgs(params);
        // 유효성검사 수행
        var _result = params.rule.validate(params.value, _args);

        result[params.ruleName] = {
            isValid: true,
            errors: []
        };
    
        // 유효성검사 결과값에 따른 result 객체 내 정보 담기
        if (typeof _result === 'object') {
            // 유효성검사 성공여부 담기
            //result.isValid = !_result.valid ? false : result.isValid;
            result.isValid = _result.valid;
            result[params.ruleName].isValid = _result.valid;
            // 결과 객체에 따른 오류메시지 담기
            if (_result.hasOwnProperty('errors')) {
                var messages = this._formatMessages(_result.errors, params);
                result.errors = result.errors.concat(messages);
                if (typeof result[params.ruleName] !== 'undefined') {
                    result[params.ruleName].errors = messages;
                }
            }
            // 결과 객체의 모든 속성을 반환 할 기본 result 객체와 병합
            for (var prop in _result) {
                if (_result.hasOwnProperty(prop) && !result.hasOwnProperty(prop)) {
                    result[params.ruleName][prop] = _result[prop];
                }
            }
        } else if (typeof _result !== 'boolean') {
            throw '[ruleName : ' + params.ruleName + '] 올바르지 않는 결과값이 반환되었습니다.';
        } else {
            //result.isValid = !_result ? false : result.isValid;
            result.isValid = _result;
            result[params.ruleName].isValid = _result;
        }
        // 유효성검사 실패 시 오류메시지 담기
        if (!result.isValid) {
            var message = _formatMessage(params);
            result.errors.push(message);
            if (typeof result[params.ruleName] !== 'undefined') {
                result[params.ruleName].errors.push(message);
            }
        }
    };
    /**
     * 속성이 규칙인지 여부를 반환
     * @ignore
     */
    var _isRule = function(rule) {
        var props = [
            'title',
            'stop',
            'ignoreNull'
        ];
        return props.indexOf(rule) < 0;
    };
    /**
     * 필요 매개변수 검사를 반복 처리
     * @ignore
     */
    var _eachExpected = function(params, fn) {
        if (Array.isArray(params.rule.expects)) {
            var expectsLength = params.rule.expects.length,
                i = expectsLength;
            // 필요변수 검사
            while (i--) {
                fn(params.rule.expects[i], expectsLength);
            }
        }
    };
    /**
     * 검사용 매개변수를 객체로 반환
     * @ignore
     */
    var _getArgs = function(params) {
        var pars = {};
        // validate 함수로 넘길 파라메터 객체 생성 (rule > expects에서 정의한 값을 토대로 생성함)
        _eachExpected(params, function (expects, expectsLength) {
            if (params.ruleValue.hasOwnProperty(expects)) {
                pars[expects] = params.ruleValue[expects];
            } else if (expectsLength <= 1 && (/^[A-Za-z0-9]+$/i.test(params.ruleValue) || toString.call(params.ruleValue) === '[object RegExp]')) {
                pars[expects] = params.ruleValue;
            } else {
                throw '[ruleName: ' + params.ruleName + '] ' + expects + ' parameter 가 필요합니다.';
            }
        });

        if (params.ruleValue.hasOwnProperty('config')) {
            pars.config = params.ruleValue.config;
        }
        return pars;
    };
    /**
     * 메시지 변환 함수
     * @ignore
     */
    var _format = function(text, col) {
        col = typeof col === 'object' ? col : Array.prototype.slice.call(arguments, 1);
        return text.replace(/\{\{|\}\}|\{(\w+)\}/g, function (m, n) {
            if (m === "{{") {
                return "{"; 
            }
            if (m === "}}") {
                return "}"; 
            }
            return col[n];
        }).trim();
    };
    /**
     * 오류 메시지의 형식을 올바르게 지정하기 위해 placholder 값을 포함하는 객체를 반환
     * @ignore
     */
    var _getFormat = function(params) {
        var format = {};
        // rule > expects에서 정의한 값이 있다면 validate 함수로 넘길 파라메터 객체 생성
        _eachExpected(params, function(expects) {
            if (params.ruleValue.hasOwnProperty(expects)) {
                format[expects] = params.ruleValue[expects];
            }
            if (/^[A-Za-z0-9]+$/i.test(params.ruleValue)) {
                format[expects] = params.ruleValue;
            }
        });
        format.title = params.title;
        return format;
    };
    /**
     * 유효성 검사 실패 시 메시지들을 반환
     * @ignore
     */
    var _formatMessages = function(errors, params) {
        var format = _getFormat(params),
            i = errors.length;
        while (i--) {
            errors[i] = _format(errors[i], format);
        }
        return errors;
    };
    /**
     * 유효성 검사 실패 시 메시지를 반환
     * @ignore
     */
    var _formatMessage = function(params) {
        var format = _getFormat(params),
            message;
        // 미리 정의된 메시지가 있다면 메시지 담기
        if (params.ruleValue.hasOwnProperty('message')) {
            message = params.ruleValue.message;
            return _format(message, format);
        } else {
            // 미리 정의된 메시지가 없다면 기본메시지로 담기
            message = params.rule.message;
            return _format(message, format);
        }
    };

    /**
     * {@link mon.validator.go} 유효성검사 실행 함수에 필요한 사용자정의 규칙 객체.<br>
     * 이 객체는 {@link mon.validator.go} 함수의 rules 인자값으로 사용된다.
     * @memberof mon.validator
     * @typedef {object} rules
     * @alias rules
     * @property {string} title - 규칙명. 검사실패 알림문구 중 {title}과 대체되어 표시 됨
     * @property {boolean} [stop=true] - 전체검사 여부<br>
     *                                   - true: 검사 실패 시 중단하고 결과를 Return<br>
     *                                   - false: 검사 실패와 관계없이 모든 규칙 검사 실행
     * @property {boolean} [ignoreNull=false] - null 무시 여부
     * 
     * @property {object|boolean} alpha - 대상 값이 영문 형식인지 검사
     *      @property {string} [alpha.message] - 사용자정의 검사실패 알림문구
     * @property {object|boolean} alphaNumeric - 대상 값이 영문 또는 숫자 형식인지 검사
     *      @property {string} [alphaNumeric.message] - 사용자정의 검사실패 알림문구
     * @property {object|string} date - 대상 값이 날짜 형식인지 검사 (정규식)
     *      @property {string} date.date - 검사 순서 ('ymd' : 년월일, 'dmy' : 일월년)
     *      @property {string} [date.message] - 사용자정의 검사실패 알림문구
     * @property {object|boolean} decimal - 대상 값이 소수 형식인지 검사
     *      @property {string} [decimal.message] - 사용자정의 검사실패 알림문구
     * @property {object|boolean} email - 대상 값이 메일 형식인지 검사
     *      @property {string} [email.message] - 사용자정의 검사실패 알림문구
     * @property {object} equal - 대상 값이 {value}값과 일치하는지 검사
     *      @property {string} equal.value - 비교대상 값
     *      @property {string} equal.field - 오류문구에 노출될 비교대상 값
     *      @property {string} [equal.message] - 사용자정의 검사실패 알림문구
     * @property {object} format - 대상 값을 정의된 정규식으로 검사
     *      @property {string} format.regex - 사용할 정규식
     *      @property {string} [format.message] - 사용자정의 검사실패 알림문구
     * @property {object|boolean} ip - 대상 값이 IP 형식인지 검사
     *      @property {string} [ip.message] - 사용자정의 검사실패 알림문구
     * @property {object|int} max - 대상 값이 {max}보다 큰 지 검사
     *      @property {int} max.max - 유효한 최대 값
     *      @property {string} [max.message] - 사용자정의 검사실패 알림문구
     * @property {object|int} min - 대상 값이 {min}보다 작은지 검사
     *      @property {int} min.min - 유효한 최소 값
     *      @property {string} [min.message] - 사용자정의 검사실패 알림문구
     * @property {object|boolean} numeric - 대상 값이 숫자 형식인지 검사
     *      @property {string} [numeric.message] - 사용자정의 검사실패 알림문구
     * @property {object} range - 대상 값이 {min}보다 작거나 {max}보다 큰 지 검사
     *      @property {int} range.min - 유효한 최대 값
     *      @property {int} range.max - 유효한 최소 값
     *      @property {string} [range.message] - 사용자정의 검사실패 알림문구
     * @property {object|boolean} required - 대상 값이 존재하는지 검사
     *      @property {string} [required.message] - 사용자정의 검사실패 알림문구
     * @property {object|boolean} time - 대상 값이 시간 형식인지 검사 (정규식)
     *      @property {string} [time.message] - 사용자정의 검사실패 알림문구
     * @property {object|boolean} url - 대상 값이 URL 형식인지 검사
     *      @property {string} [url.message] - 사용자정의 검사실패 알림문구
     * 
     * @example
     * //---------------------------------------
     * //   alpha rule
     * //---------------------------------------
     * var rules = {
     *      alpha: true
     * };
     * var result = mon.validator.go('alphabet', rules);
     * // Result { isValid: true, errors: [], each: function }
     * var rules = { 
     *      alpha: { message: '알파벳 형식만 가능합니다.' }
     * }
     * var result = mon.validator.go('alphabet123', rules);
     * // Result { isValid: true, errors: ["알파벳 형식만 가능합니다."], each: function }
     * 
     * //---------------------------------------
     * //   alphaNumeric rule
     * //---------------------------------------
     * var rules = {
     *      alphaNumeric: true
     * };
     * var result = mon.validator.go('alphabet 123', rules);
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   date rule
     * //---------------------------------------
     * var rules = {
     *      title: '등록일자',
     *      date: 'ymd'
     * };
     * var result = mon.validator.go('2017-08-08', rules);
     * // Result { isValid: true, errors: [], each: function }
     * var result = mon.validator.go('2017-08-00', rules);
     * // Result { isValid: true, errors: ["등록일자은(는) 날짜형식이어야 합니다."], each: function }
     * 
     * //---------------------------------------
     * //   decimal rule
     * //---------------------------------------
     * var rules = {
     *      decimal: true
     * };
     * var result = mon.validator.go('12.3', rules);
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   email rule
     * //---------------------------------------
     * var rules = {
     *      email: true
     * };
     * var result = mon.validator.go('user@domain.com', rules);
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   equal rule
     * //---------------------------------------
     * var rules = {
     *      title: '비밀번호',
     *      equal: {
     *          value: '1234',
     *          field: '12**'
     *      }
     * };
     * var result = mon.validator.go('1111', rules);
     * // Result { isValid: false, errors: ["비밀번호이(가) '12**'이(가) 일치하지 않습니다."], each: function }
     * var result = mon.validator.go('1234', rules);
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   format rule
     * //---------------------------------------
     * var rules = {
     *      title: '제목',
     *      format: {
     *          regex: /^[A-Za-z ]+$/
     *      }
     * };
     * var result = mon.validator.go('안녕하세요.', rules);
     * // Result { isValid: false, errors: ["제목이(가) 정규식[/^[A-Za-z ]+$/] 검사에 실패했습니다.], each: function }
     * var result = mon.validator.go('good morning', rules);
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   ip rule
     * //---------------------------------------
     * var rules = {
     *      ip: true
     * };
     * var result = mon.validator.go('192.168.0.1', rules);
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   max rule
     * //---------------------------------------
     * var rules = {
     *      max: 10
     * };
     * var result = mon.validator.go('안녕하세요. 정말 좋은 아침입니다.', rules);
     * // Result { isValid: false, errors: ["최대 10자를 넘었습니다."], each: function }
     * 
     * //---------------------------------------
     * //   min rule
     * //---------------------------------------
     * var rules = {
     *      min: 5
     * };
     * var result = mon.validator.go('안녕하세요. 반갑습니다.', rules);
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   numeric rule
     * //---------------------------------------
     * var rules = {
     *      numeric: true
     * };
     * var result = mon.validator.go('123', rules);
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   range rule
     * //---------------------------------------
     * var rules = {
     *     title: '글제목',    // 메시지에 표시할 타이틀
     *     range: { 
     *         min: 5,
     *         max: 10
     *     }
     * };
     * console.log(mon.validator.go('안녕', rules));
     * // Result { isValid: false, errors: ["글제목은(는) 최소5자 이상, 최대10자 이하이어야 합니다."], each: function }
     * console.log(mon.validator.go('안녕하세요. 반갑습니다. 감사합니다.', rules));
     * // Result { isValid: false, errors: ["글제목은(는) 최소5자 이상, 최대10자 이하이어야 합니다."], each: function }
     * console.log(mon.validator.go('안녕하세요.', rules));
     * // Result { isValid: true, errors: [], each: function }
     * 
     * //---------------------------------------
     * //   required rule
     * //---------------------------------------
     * var rules = {
     *      title: '제목',
     *      required: true
     * };
     * var result = mon.validator.go('value', rules);
     * // Result { isValid: false, errors: ["제목이(가) 필요합니다."], each: function }
     * 
     * //---------------------------------------
     * //   time rule
     * //---------------------------------------
     * var rules = {
     *      title: '등록시간',
     *      time: true
     * };
     * var result = mon.validator.go('10:59:59', rules);
     * // Result { isValid: true, errors: [], each: function }
     * var result = mon.validator.go('1:59:59', rules);
     * // Result { isValid: true, errors: [], each: function }
     *  var result = mon.validator.go('10:59:60', rules);
     * // Result { isValid: false, errors: ["등록시간은(는) 시간형식이어야 합니다."], each: function }
     * 
     * //---------------------------------------
     * //   url rule
     * //---------------------------------------
     * var rules = {
     *      url: { message: '올바른 URL 주소형식이 아닙니다.' }
     * };
     * var result = mon.validator.go('http://www.domain.com', rules);
     * // Result { isValid: true, errors: [], each: function }
     * var result = mon.validator.go('domain', rules);
     * // Result { isValid: false, errors: ["올바른 URL 주소형식이 아닙니다."], each: function }
     * var result = mon.validator.go('domain', {
     *     title: '홈페이지 주소',
     *     url: true
     * });
     * // Result { isValid: false, errors: ["홈페이지 주소은(는) URL 주소 형식이어야 합니다."], each: function }
     */
    var rules = {
        alpha: {
            regex: /^[A-Za-z]+$/,
            validate: function(value) {
                return this.regex.test(value);
            },
            message: '{title}은(는) 영문이어야 합니다.',
            expects: false
        },
        alphaNumeric: {
            regex: /^[A-Za-z0-9]+$/i,
            validate: function (value) {
                return this.regex.test(value);
            },
            message: '{title}은(는) 영문 또는 숫자이어야 합니다.',
            expects: false
        },
        date: {
            formats: {
                ymd: /^(?:\2)(?:[0-9]{2})?[0-9]{2}([\/-])(1[0-2]|0?[1-9])([\/-])(3[01]|[12][0-9]|0?[1-9])$/,
                dmy: /^(3[01]|[12][0-9]|0?[1-9])([\/-])(1[0-2]|0?[1-9])([\/-])(?:[0-9]{2})?[0-9]{2}$/
            },
            validate: function (value, pars) {
                return this.formats[pars.format].test(value);
            },
            message: '{title}은(는) 날짜형식이어야 합니다.',
            expects: ['format']
        },
        decimal: {
            regex: /^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/,
            validate: function(value) {
                return this.regex.test(value);
            },
            message: '{title}은(는) 소수이어야 합니다.',
            expects: false
        },
        email: {
            regex: /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i, // eslint-disable-line no-control-regex
            validate: function(value) {
                return this.regex.test(value);
            },
            message: '{title}은(는) 메일주소 형식이어야 합니다.',
            expects: false
        },
        equal: {
            validate: function(value, pars) {
                return '' + value === '' + pars.value;
            },
            message: '{title}과(와) {field}이(가) 일치하지 않습니다.',
            expects: ['value', 'field']
        },
        format: {
            validate: function (value, pars) {
                if (Object.prototype.toString.call(pars.regex) === '[object RegExp]') {
                    return pars.regex.test(value);
                }
                throw '[format] - regex is not a valid regular expression.';
            },
            message: '{title}이(가) 정규식[{regex}] 검사에 실패했습니다.',
            expects: ['regex']
        },
        ip: {
            regex: {
                ipv4: /^(?:(?:\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(?:\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])$/,
                ipv4Cidr: /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|[1-2][0-9]|3[0-2]))$/,
                ipv6: /^((?=.*::)(?!.*::.+::)(::)?([\dA-F]{1,4}:(:|\b)|){5}|([\dA-F]{1,4}:){6})((([\dA-F]{1,4}((?!\3)::|:\b|$))|(?!\2\3)){2}|(((2[0-4]|1\d|[1-9])?\d|25[0-5])\.?\b){4})$/i,
                ipv6Cidr: /^s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]d|1dd|[1-9]?d)(.(25[0-5]|2[0-4]d|1dd|[1-9]?d)){3}))|:)))(%.+)?s*(\/([0-9]|[1-9][0-9]|1[0-1][0-9]|12[0-8]))?$/
            },
            validate: function (value) {
                return this.regex.ipv4.test(value) || this.regex.ipv6.test(value) || this.regex.ipv4Cidr.test(value) || this.regex.ipv6Cidr.test(value);
            },
            message: '{title}은(는) IP형식이어야 합니다.',
            expects: false
        },
        max: {
            validate: function(value, pars) {
                return typeof value === 'string' && value.length <= pars.max;
            },
            message: '{title}은(는) 최대{max}자 이하이어야 합니다.',
            expects: ['max']
        },
        min: {
            validate: function(value, pars) {
                return typeof value === 'string' && value.length >= pars.min;
            },
            message: '{title}은(는) 최소{min}자 이상이어야 합니다.',
            expects: ['min']
        },
        numeric: {
            regex: /^-?[0-9]+$/,
            validate: function(value) {
                return this.regex.test(value);
            },
            message: '{title}은(는) 숫자이어야 합니다.',
            expects: false
        },
        range: {
            validate: function(value, pars) {
                if (typeof value === 'string') {
                    return value.length >= pars.min && value.length <= pars.max;
                } else if (typeof value === 'number') {
                    return value >= pars.min && value <= pars.max;
                }
                return false;
            },
            message: '{title}은(는) 최소{min}자 이상, 최대{max}자 이하이어야 합니다.',
            expects: ['min', 'max']
        },
        required: {
            validate: function(value) {
                return !!value;
            },
            message: '{title}이(가) 필요합니다.',
            expects: false
        },
        time: {
            regex: /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]):([0-5]?[0-9])$/,
            validate: function (value) {
                return this.regex.test(value);
            },
            message: '{title}은(는) 시간형식이어야 합니다.',
            expects: false
        },
        url: {
            regex: /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/i,
            validate: function(value) {
                return this.regex.test(value);
            },
            message: '{title}은(는) URL 주소 형식이어야 합니다.',
            expects: false
        }
    };
    /**
     * 유효성검사를 실행한다.
     * @memberof mon.validator
     * @method go
     * @param {*} value - 대상 값
     * @param {rules} rules - 유효성검사 규칙 객체
     * @returns {result} - 유효성검사 결과 객체
     * @example
     * // 규칙정의
     * var rules = {
     *     title: '글제목',    // 메시지에 표시할 타이틀
     *     required: true,     // 필수여부 규칙
     *     max: { max: 10, message: '최대 10자를 넘었습니다.' },   // 최대값 검사 규칙
     *     min: { min: 5 }                                         // 최소값 검사 규칙
     * };
     * // 검사처리
     * mon.validator.go('', rules);
     * // Result { isValid: false, errors: ["글제목이(가) 필요합니다."], each: function }
     * mon.validator.go('안녕!', rules);
     * // Result { isValid: false, errors: ["글제목은(는) 최소5자 이상이어야 합니다"], each: function }
     * mon.validator.go('안녕하세요. 정말 좋은 아침입니다.', rules);   
     * // Result { isValid: false, errors: ["최대 10자를 넘었습니다."], each: function }
     * mon.validator.go('안녕하세요.', rules);                         
     * // Result { isValid: true, errors: [], each: function }  
     */
    var go = function (value, rules) {
        if (typeof rules !== 'object') {
            throw '유효성 검사에 필요한 규칙이 없습니다.';
        }
        // result 객체를 리턴
        return _start(value, rules);
    };
    /**
     * 사용자정의 규칙을 추가한다.
     * @memberof mon.validator
     * @method addRule
     * @param {object} rule 사용자정의 규칙 객체
     * @param {string} name 사용자정의 규칙명
     */
    var addRule = function(rule, name) {
        // 타입검사
        if (!type.isObject(rule)) {
            throw '규칙 타입이 잘못 되었습니다.';
        }
        try {
            // 규칙이 이미 존재하는지 검사
            if (!rules.hasOwnProperty(name)) {
                // 규칙이 이미 존재하지 않으면 추가
                rules[name] = rule;
            }
        } catch (e) {
            throw 'mon.validator.addRule(): ' + e.message;
        }
    };

    return {
        rules: rules,
        go: go,
        addRule: addRule
    };
})();

if (typeof module !== 'undefined' && module.exports) {
    module.exports = validator;
} else if (typeof mon !== 'undefined') {
    if (typeof Object.assign !== 'undefined') {
        Object.assign(mon, { validator: validator });
    } else {
        var extend = function() {
            var extended = {},
                key, prop;
            for(key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        extend(mon, { validator: validator });
    }
} else {
    var mon = mon || {};
    mon.validator = validator;
}