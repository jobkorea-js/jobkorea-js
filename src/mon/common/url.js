/**
 * @author Jobkorea Development Lab
 * @desc 알바몬 url 관련 모듈
 * @dependency core.js
 * @namespace mon.url
 */
var url = (function () {

    'use strict';

    var type = require('./type');

    /**
     * queryString의 정보를 객체로 가져온다.
     * @memberof mon.url
     * @param {string} [query=''] - 변환할 query string. 없을경우 현재 주소의 query string으로 가져온다.
     * @returns {object} queryString 객체
     * @example
     * mon.url.parseQuery()             // (www.albamon.com?q1=1&q2=2 에서) {q1: '1, q2: '2'}
     * mon.url.parseQuery('q1=1&q2=2')  // {q1: '1, q2: '2'}
     */
    var parseQuery = function (query) {
        var result = {},
            search = type.isEmpty(query) ? window.location.search.substr(1) : query,
            searchs, pair;

        if (type.isEmpty(search)) return '';
        
        searchs = search.split('&');
        searchs.forEach(function(element) {
            pair = element.split('=');
            result[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
        }, this);
        
        return result;
    };

    /**
     * 객체를 queryString 형식으로 가져온다.
     * @memberof mon.url
     * @param {object} obj - 변경할 queryString객체. 생략 시 현재 주소에 있는 queryString객체를 가져온다.
     * @returns {string} queryString 문자열
     * @example
     * mon.url.paramterizeQuery()                          // (www.albamon.com?qs=1&level=2 에서) qs=1&level=2
     * mon.url.paramterizeQuery({test1: 1, test2: 2})      // test=1&level=2
     */
    var paramterizeQuery = function (obj) {
        var query = [], key;
        if (type.isEmpty(obj)) obj = parseQuery;
        if (type.isEmpty(obj)) return '';

        for (key in obj) {
            if (obj.hasOwnProperty(key)) {
                query.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
            }
        }
        return query.join('&');
    };

    return {
        parseQuery: parseQuery,
        paramterizeQuery: paramterizeQuery
    };
})();

if (typeof module !== 'undefined' && module.exports) {
    module.exports = url;
} else if (typeof mon !== 'undefined') {
    if (typeof Object.assign !== 'undefined') {
        Object.assign(mon, { url: url });
    } else {
        var extend = function() {
            var extended = {},
                key, prop;
            for(key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        extend(mon, { url: url });
    }
} else {
    var mon = mon || {};
    mon.url = url;
}