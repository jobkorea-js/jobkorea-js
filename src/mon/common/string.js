/**
 * @author Jobkorea Development Lab
 * @desc 알바몬 사이트 domain 관련 모듈
 * @dependency core.js, type.js
 * @namespace mon.string
 */
var string = (function() {
    'use strict';

    var type = require('./type');

    /**
     * 문자열을 {cutLen}만큼 자른다.
     * @memberof mon.string
     * @method cut
     * @param {string} target 대상 문자
     * @param {int} cutLen 자를 길이
     * @returns {string} 결과 문자열
     * @example
     * mon.string.cut('abcdef', 2) // "ab"
     */
    var cut = function (target, cutLen) {
        var str = target,
            i = 0,
            length = target.length, 
            l = 0;

        for (i; i < length; i += 1) {
            var c = str.charCodeAt(i);
            l += c >> 11 ? 2 : c >> 7 ? 2 : 1;
            if (l > cutLen) return str.substring(0, i);
        }

        return str;
    };

    /**
     * 대상 문자열을 포멧형태로 변환한다. (C#의 String.Format과 유사)
     * @memberof mon.string
     * @method format
     * @param {string} target 대상 문자. {n}으로 변환할 위치를 지정한다.
     * @param {...*} args 치환할 문자들
     * @returns {string} 결과 문자열
     * @example
     * mon.string.format('{0}년{1}월', 2017, 8) // "2017년8월"
     */
    var format = function (target, args) {
        var str = target,
            i = 0,
            length = arguments.length;

        for (i; i < length; i+=1) {
            var regEx = new RegExp("\\{" + i + "\\}", "gm");
            var changeString = arguments[i+1];
            if (!type.isExist(changeString)) changeString = "";
            str = str.replace(regEx, changeString);
        }

        return str;
    };

    /**
     * 대상 문자열의 Byte 길이를 구한다.
     * @memberof mon.string
     * @method getByteSize
     * @param {string} target 대상 문자
     * @returns {int} 문자열의 Byte 길이
     * @example
     * mon.string.getByteSize('abcde') // 6
     * mon.string.getByteSize('한글')  // 6
     */
    var getByteSize = function (target) {
        var result;

        result = (function(s,b,i,c){
            for (b = i = 0; c = s.charCodeAt(i++); b += c >> 11 ? 3 : c >> 7 ? 2 : 1);
            return b;
        })(target);

        return result;
    };

    /**
     * 대상 문자열의 한글 포함여부를 가져온다.
     * @memberof mon.string
     * @method isKorean
     * @param {string} target 대상 문자
     * @returns {string} 한글 포함여부
     * @example
     * mon.string.isKorean('abcde') // false
     * mon.string.isKorean('abcde한글')  // true
     */
    var isKorean = function (target) {
        var iCode = 0, 
            cChar = "",
            i = 0,
            len = target.length;

        for (i; i < len; i+=1) {
            iCode = parseInt(target.charCodeAt(i));
            cChar = target.substr(i, 1).toUpperCase();
            if ((cChar < "0" || cChar > "9") && (cChar < "A" || cChar > "Z") && ((iCode > 255) || (iCode < 0))) {
                return true;
            }
        }
        return false;
    };

    /**
     * 대상 문자열이 날짜형식인지 검사한다.
     * @memberof mon.string
     * @method isDate
     * @param {string} target 대상 문자
     * @returns {boolean} 날짜형식 여부
     * @example
     * mon.string.isDate('2017-01-01');    // true
     * mon.string.isDate('20170101');      // true
     * mon.string.isDate('9999-99-99');    // false
     * mon.string.isDate('2017320');       // false
     * mon.string.isDate('2017-03-20');    // true
     */
    var isDate = function (target) {
        var date = target.replace(/[^0-9]/g, '');
        if (date.length == 8) {
            var year = Number(date.substring(0, 4));
            var month = Number(date.substring(4, 6));
            var day = Number(date.substring(6, 8));
        } else if (date.length == 6) {
            var year = Number('19' + date.substring(0, 2));
            var month = Number(date.substring(2, 4));
            var day = Number(date.substring(4, 6));
        } else {
            return false; 
        }

        var rgMaxDay = [31, ((((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (month < 1 || month > 12) {
            return false; 
        }
        if (day < 1 || day > rgMaxDay[month - 1]) {
            return false; 
        }
        return true;
    };

    /**
     * 전체 문자열 치환.
     * 대상 문자열{target}중 {org}들을 모두를 {dest}문자로 변경한다.
     * @memberof mon.string
     * @method replaceAll
     * @param {string} target 대상 문자
     * @param {string} org 변경전 문자
     * @param {string} dest 변경할 문자
     * @returns {string} 결과 문자열
     * @example
     * mon.string.replaceAll('tomato','t','*'); // '*oma*o'
     * mon.string.replaceAll('트위스트','트','*'); '*위스*'
     */
    var replaceAll = function (target, org, dest) { 
        var args = arguments, length = args.length;
        var regExp = new RegExp(args[1], "g");
        if (length <= 1) {
            return target;
        }
        if (length == 2) {
            return target.replace(regExp, ""); 
        } else {
            return target.replace(regExp, dest); 
        }
        return target;
    };

    /**
     * 정의된 태그{allowed}를 제외한 나머지 태그들을 제거한다.
     * @memberof mon.string
     * @method stripTag
     * @param {string} target 대상 문자
     * @param {string} allowed 허용할 문자
     * @returns {string} 결과 문자열
     * @example
     * mon.string.stripTag('<img src="/images/myImage.png"><br><p>이건 나의 이미지 입니다.</p><span>(새로운이미지)</span>','<img><br><span>'); // "<img src="/images/myImage.png"><br>이건 나의 이미지 입니다.<span>(새로운이미지)</span>"
     * mon.string.stripTag('<img src="/images/myImage.png"><br><p>이건 나의 이미지 입니다.</p><span>(새로운이미지)</span>','<img><br>'); // "<img src="/images/myImage.png"><br>이건 나의 이미지 입니다.(새로운이미지)"
     * mon.string.stripTag('<img src="/images/myImage.png"><br><p>이건 나의 이미지 입니다.</p><span>(새로운이미지)</span>','<img><br>'); // "이건 나의 이미지 입니다.(새로운이미지)"
     */
    var stripTag = function (target, allowed) {
        allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
        var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
            commentsAndTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
        return target.replace(commentsAndTags, '').replace(tags, function ($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
        });
    };

    /**
     * 대상 문자열의 앞뒤 공백을 제거한다.
     * @memberof mon.string
     * @method trim
     * @param {string} target 대상 문자
     * @returns {string} 결과 문자열
     * @example
     * mon.string.trim(' 대한민국 만세 '); // "대한민국 만세"
     */
    var trim = function (target) {
        return target.replace(/^\s+|\s+$/g, '');
    };

    /**
     * 대상 문자열의 모든 공백을 제거한다.
     * @memberof mon.string
     * @method trimAll
     * @param {string} target 대상 문자
     * @returns {string} 결과 문자열
     * @example
     * mon.string.trimAll(' 대한민국 만세 '); // "대한민국만세"
     */
    var trimAll = function (target) {
        return target.replace(/\s*/g, ''); 
    };

    /**
     * 대상 문자열의 왼쪽 공백을 제거한다.
     * @memberof mon.string
     * @method trimStart
     * @param {string} target 대상 문자
     * @returns {string} 결과 문자열
     * @example
     * mon.string.trimLeft(' 대한민국 만세 '); // "대한민국 만세 "
     */
    var trimStart = function (target) {
        return target.replace(/^\s*/g, ''); 
    };

    /**
     * 대상 문자열의 오른쪽 공백을 제거한다.
     * @memberof mon.string
     * @method trimEnd
     * @param {string} target 대상 문자
     * @returns {string} 결과 문자열
     * @example
     * mon.string.trimRight(' 대한민국 만세 '); // " 대한민국 만세"
     */
    var trimEnd = function (target) {
        return target.replace(/\s*$/g, ''); 
    };
    
    /**
     * 대상 문자열의 길이를 제한하고 단축문자열을 추가한다.
     * @memberof mon.string
     * @method trunc
     * @param {string} target 대상 문자
     * @param {int} maxLength 최대 길이
     * @param {string} opts.ellipsis 대체 문자
     * @returns {string} 결과 문자열
     * @example
     * mon.string.trunc('대한민국 만세', 5); // "대한민국 만세"
     */
    var trunc = function (target, maxLength, opts) {
        var isTooLong = target.length > maxLength,
            result = isTooLong ? target.substr(0, maxLength) : target,
            tail = '...';

        if (type.isEmpty(opts) === false) {
            tail = type.isEmpty(opts.ellipsis) ? tail : opts.ellipsis;
        }

        return isTooLong ? this.trim(result) + tail : result;
    };

    /**
     * 대상 문자열의 모든 태그를 제거한다.
     * @memberof mon.string
     * @method stripTags
     * @param {string} target 대상 문자
     * @param {boolean} isStripContents 내용 삭제 여부
     * @returns {string} 결과 문자열
     * @example
     * mon.string.stripTags('<img src="/images/myImage.png"><br><p>이건 나의 이미지 입니다.</p><span>(새로운이미지)</span>'); // "이건 나의 이미지 입니다.(새로운이미지)"
     * mon.string.stripTags('<img src="/images/myImage.png"><br><p>이건 나의 이미지 입니다.</p><span>(새로운이미지)</span>', true); // ""
     */
    var stripTags = function (target, isStripContents) {
        var result = '';
        if (isStripContents) {
            result = target.replace(/<([^>]+?)([^>]*?)>(.*?)<\/\1>/ig, "");
            result = result.replace(/<([^>]+?)([^>]*?)>/ig, "");
            result = result.replace(/\s*$/g, "");
        } else {
            result = target.replace(/<.*?>/g, '');
        }
        return result;
    };

    /**
     * 대상 문자열의 왼쪽값을 {contents}로 {range}만큼 모두 채운다.
     * @memberof mon.string
     * @method padLeft
     * @param {string} target 대상 문자
     * @param {string} contents 내용
     * @param {int} range 유효범위
     * @returns {string} 결과 문자열
     * @example
     * mon.string.padLeft('1', '0', 2); // "02"
     */
    var padLeft = function (target, contents, range) {
        if (!target || !contents || target.length >= range) {
            return target;
        }
        var max = (range - target.length) / contents.length;
        for (var i = 0; i < max; i++) {
            target = contents + target;
        }
        return target;
    };

    //오른쪽값채우기 ex: "1".rpad("0", 2), result: "10"
    /**
     * 대상 문자열의 오른쪽값을 {contents}로 {range}만큼 모두 채운다.
     * @memberof mon.string
     * @method padRight
     * @param {string} target 대상 문자
     * @param {string} contents 내용
     * @param {int} range 유효범위
     * @returns {string} 결과 문자열
     * @example
     * mon.string.padRight('1', '0', 2); // "10"
     */
    var padRight = function (target, contents, range) {
        if (!target || !contents || target.length >= range) {
            return target;
        }
        var max = (range - target.length) / contents.length;
        for (var i = 0; i < max; i++) {
            target += contents;
        }
        return target;
    };

    return {
        cut: cut,
        format: format,
        getByteSize: getByteSize,
        isKorean: isKorean,
        isDate: isDate,
        replaceAll: replaceAll,
        stripTag: stripTag,
        trim: trim,
        trimAll: trimAll,
        trimStrat: trimStart,
        trimEnd: trimEnd,
        trunc: trunc,
        stripTags: stripTags,
        padLeft: padLeft,
        padRight: padRight
    }; 
})();

if (typeof module !== 'undefined' && module.exports) {
    module.exports = string;
} else if (typeof mon !== 'undefined') {
    if (typeof Object.assign !== 'undefined') {
        Object.assign(mon, { string: string });
    } else {
        var extend = function() {
            var extended = {},
                key, prop;
            for(key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        extend(mon, { string: string });
    }
} else {
    var mon = mon || {};
    mon.string = string;
}