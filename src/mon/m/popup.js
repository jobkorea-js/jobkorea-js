/**
 * @author Jobkorea Development Lab
 * @desc 알바몬 popup 관련 모듈
 * @dependency core.js
 * @ignore
 */
var popup = (function () {
    'use strict';

    var mon = require('../../core/core');
    var type = require('../common/type');
    var _url = require('../common/url');
    var func = require('../common/function');
    var array = require('../common/array');
    var _popupId = 0;

    /**
     * 팝업 관리자 객체
     * @constructor
     * @memberof mon
     * @example
     * var popup = mon.Popup;
     * popup.open('http://www.albamon.com/list/gi/mon_gib_read.asp', {
     *     name: 'albamon',
     *     method: 'get', 
     *     specs: 'width=1000,height=800', 
     *     param: {AL_GI_No:'1000000'}
     * });
     */
    var Popup = function () {
        /**
         * 현재 띄워진 팝업창들
         * @type {object}
         */
        this.openedPopup = {};
        /**
         * 부모창 페이지변경 시 팝업 자동닫기 처리 여부
         * @type {object}
         */
        this.isCloseWithParentPopup = {};
    };

    /**
     * 폼객체 생성
     * @private
     * @param {string} url - form 주소
     * @param {object} param - url queryString
     * @param {string} method - form method type
     * @param {string} target - form target
     * @param {object} _document - 문서 객체
     * @param {object} container - 삽입할 객체
     * @returns {object} 생성된 폼 객체
     */
    Popup.prototype._createForm = function(url, param, method, target, _document, container) {
        try {
            _document = type.isExist(_document) ? _document : document;

            var form = _document.createElement('form'),
                key,
                input;
            container = container || _document.body;
            form.method = method || 'POST';
            form.action = url || '';
            form.target = target || '';
            form.style.display = 'none';

            for (key in param) {
                if (param.hasOwnProperty(key)) {
                    input = _document.createElement('input');
                    input.name = key;
                    input.type = 'hidden';
                    input.value = param[key];
                    form.appendChild(input);
                }
            }
            container.appendChild(form);
            return form;            
        } catch (e) {
            console.log('Error:' + e.message);
        }
    };

    /**
     * 팝업창을 띄운다.
     * @private
     * @param {string} name - 팝업명
     * @param {string} url - 팝업창 주소
     * @param {string} specs - 팝업 옵션
     * @param {boolean} isIEPost - IE11 여부
     * @returns {object} 팝업객체
     */
    Popup.prototype._open = function (name, url, specs, isIEPost) {
        var _popup;
        if (isIEPost) {
            _popup = window.open('about:blank', name, specs);
            if (type.isExist(localStorage['postParam'])) {
                setTimeout(func.bind(function () {
                    var param = _url.parseQuery(localStorage['postParam']);
                    var formElement = this._createForm(url, param, 'POST', name, _popup.document);

                    if (_popup) {
                        formElement.submit();
                    }
                }, this), 100);
            }
        } else {
            _popup = window.open(url, name, specs);
        }
        return _popup;
    };

    /**
     * 팝업객체를 가져온다.
     * @param {string|int} [key] - 팝업객체 키. 생략 시 전체 팝업리스트를 가져온다.
     * @returns {object} 팝업 객체
     * @example
     * var Popup = mon.Popup;
     * Popup.open('http://www.albamon.com/list/gi/mon_gib_read.asp', {
     *     name: 'albamon',
     *     method: 'get', 
     *     specs: 'width=1000,height=800', 
     *     param: {AL_GI_No:'1000000'}
     * });
     * var openedPopup = Popup.getPopup('albamon');   // openedPopup = Window {window:Window, ...}   
     * var openedPopup = Popup.getPopup(0);           // openedPopup = Window {window:Window, ...}   
     */
    Popup.prototype.getPopup = function (key) {
        var _popup, i = 0;
        if (type.isExist(key)) {
            if (type.isNumber(key)) {
                array.each(this.openedPopup, function (index, item) {
                    if (i === key) {
                        _popup = item;
                    }
                    i = i + 1;
                });
            } else {
                _popup = this.openedPopup[key];
            }
        } else {
            _popup = this.openedPopup;
        }
        return _popup;
    };

    /**
     * 팝업창을 닫는다.  (부모창과 같은 도메인내에서만 가능)
     * @param {boolean} isCloseBeforeUnload - 부모창 페이지변경 시 팝업 자동닫기 처리 여부
     * @param {object} popup - 대상 팝업객체
     * @example
     * var Popup = mon.Popup;
     * Popup.open('http://www.albamon.com/list/gi/mon_gib_read.asp', {
     *     name: 'albamon',
     *     method: 'get', 
     *     specs: 'width=1000,height=800', 
     *     param: {AL_GI_No:'1000000'}
     * });
     * Popup.close(true, Popup.getPopup(0));
     * Popup.close(true, Popup.getPopup('albamon'));
     */
    Popup.prototype.close = function (isCloseBeforeUnload, popup) {
        try {
            var target = popup || window;
            isCloseBeforeUnload = type.isExist(isCloseBeforeUnload) ? isCloseBeforeUnload : false;
            // 페이지이동 시 닫기처리 바인딩 제거
            if (isCloseBeforeUnload === false) {
                window.onbeforeunload = null;
            }
            // 닫기 처리
            if (target.closed === false) {
                target.opener = window.location.href;
                target.close();
                delete this.openedPopup[target.name]; // 오픈팝업객체에서 닫은창 제거
            }
        } catch (e) {
            // for cross domain deny.
            console.log('Error:' + e.message);
        }
    };

    /**
     * 모든 팝업창을 닫는다.  (부모창과 같은 도메인내에서만 가능)
     * @param {boolean} isCloseWithParent - 부모창이 닫힐때 같이 닫는지 여부
     * @example
     * var Popup = mon.Popup;
     * Popup.open('http://www.albamon.com/list/gi/mon_gib_read.asp', {
     *     name: 'albamon',
     *     method: 'get', 
     *     specs: 'width=1000,height=800', 
     *     param: {AL_GI_No:'1000000'}
     * });
     * Popup.closeAll();
     */
    Popup.prototype.closeAll = function (isCloseWithParent) {
        var key;
        isCloseWithParent = type.isExist(isCloseWithParent) ? isCloseWithParent : false;
        
        for (key in this.openedPopup) {
            if (this.openedPopup.hasOwnProperty(key)) {
                if ((isCloseWithParent && this.isCloseWithParentPopup[key]) || isCloseWithParent === false) {
                    this.close(true, this.openedPopup[key]);
                }
            }
        }
    };

    /**
     * 팝업창을 활성화 한다.
     * @param {string|int} key - 팝업창명
     * @example
     * var Popup = mon.Popup;
     * Popup.open('http://www.albamon.com/list/gi/mon_gib_read.asp', {
     *     name: 'albamon',
     *     method: 'get', 
     *     specs: 'width=1000,height=800', 
     *     param: {AL_GI_No:'1000000'}
     * });
     * Popup.focus('albamon');
     */
    Popup.prototype.focus = function (key) {
        var target = this.getPopup(key);
        if (type.isExist(target)) target.focus();
    };

    /**
     * 팝업창을 연다.
     * @param {string} url - 팝업창 주소
     * @param {object} opts - 옵션값
     * @param {string} [opts.name='popup_{시간넘버}'] - 팝업창 명. 생략시 새로운 이름이 자동으로 지정된다.
     * @param {string} [opts.specs] - 팝업창 옵션
     * @param {string} [opts.method='GET'] - 팝업창 url 전송방식
     * @param {object} [opts.param] - 팝업창 url queryString
     * @param {boolean} [opts.isReload=true] - 팝업창 새로고침 여부
     * @param {boolean} [opts.isCloseWithParentPopup=true] - 페이지 이동시 모든 팝업 닫기 처리 여부 (부모창과 같은 도메인내에서만 가능)
     * @returns {object} 팝업 인스턴스
     * @example
     * var Popup = mon.Popup;
     * Popup.open('http://www.albamon.com/list/gi/mon_gib_read.asp', {
     *     name: 'albamon',
     *     method: 'get', 
     *     specs: 'width=1000,height=800', 
     *     param: {AL_GI_No:'1000000'}
     * });
     */
    Popup.prototype.open = function (url, opts) {
        var popup, formElement, _param, isIEPost;
        opts = Object.assign({
            name: 'popup_' + _popupId + Number(new Date()),
            specs: '',
            method: 'GET',
            param: {},
            isReload: true,
            isCloseWithParentPopup: true
        }, opts || {});

        opts.method = opts.method.toUpperCase();

        // IE 11 이상버전 체크
        if (opts.method === 'POST' && opts.param && mon.browser.name === 'Internet Explorer') {
            if (type.isExist(mon.browser.version)) {
                if (parseInt(mon.browser.version.split('.')[0]) >= 11) {
                    isIEPost = true;
                }
            }
        }

        if (type.isExist(url) === false) {
            throw new Error('popup url이 존재 하지 않습니다.');
        }

        // 팝업아이디 증가
        _popupId += 1;

        // 파라메터 처리
        if (opts.param) {
            if (opts.method === 'GET') {
                _param = _url.paramterizeQuery(opts.param);
                url = type.isEmpty(_param) ? url : url + (/\?/.test(url) ? '&' : '?') + _param;
            } else if (opts.method === 'POST') {
                /* 
                    IE11 이상일 경우 관리자모드나 브라우저 보안상태에 따라 새탭으로 열리는 경우가 있어
                    IE11 이상은 부모창에서 form submit 방식을 사용하지 않고 빈 팝업창(about:blank)을 연 후 
                    팝업창에서 post submit을 하는 방식을 사용한다.
                */
                if (isIEPost === false) {
                    formElement = this._createForm(url, opts.param, opts.method, opts.name);
                    url = 'about:blank';
                }
            }
        }

        // 오픈팝업배열에 담기
        popup = this.openedPopup[opts.name];

        // 오픈팝업배열 열기
        if (type.isExist(popup) === false) {
            this.openedPopup[opts.name] = popup = this._open(opts.name, url, opts.specs, isIEPost);
        } else if (popup.closed) {
            this.openedPopup[opts.name] = popup = this._open(opts.name, url, opts.specs, isIEPost);
        } else {
            if (opts.isReload) {
                popup.location.replace(url);
            }
            popup.focus();
        }

        // 부모팝업창 같이 닫기 여부 담기
        this.isCloseWithParentPopup[opts.name] = opts.isCloseWithParentPopup;

        // opts.method가 post일경우 처리
        if (opts.param && opts.method === 'POST' && isIEPost === false) {
            if (popup) {
                formElement.submit();
            }
            if (formElement.parentNode) {
                formElement.parentNode.removeChild(formElement);
            }
        }
        
        // 페이지 이동시 모든 팝업 닫기 처리
        window.onbeforeunload = func.bind(this.closeAll, this);
    };

    return new Popup();
})();

if (typeof module !== 'undefined' && module.exports) {
    module.exports = popup;
} else if (typeof mon !== 'undefined') {
    if (typeof Object.assign !== 'undefined') {
        Object.assign(mon, { popup: popup });
    } else {
        var extend = function() {
            var extended = {},
                key, prop;
            for(key in arguments) {
                var argument = arguments[key];
                for (prop in argument) {
                    if (Object.prototype.hasOwnProperty.call(argument, prop)) {
                        extended[prop] = argument[prop];
                    }
                }
            }
            return extended;
        };
        extend(mon, { popup: popup });
    }
} else {
    var mon = mon || {};
    mon.type = popup;
}