/**
 * 알바몬 메인 객체
 * @author Jobkorea Development Lab
 * @version 1.0.1
 * @dependency core.js
 * jsdoc 생성 명령어 : jsdoc ./sources/src/mon/common ./sources/src/mon/m ./sources/src/core/core.js ./sources/src/core/polyfill.js -d ./docs/albamon/m/
 * @namespace mon
 */
'use strict';

var mon = require('../../core/core');
var Promise = require('../../core/polyfill');

/** 알바몬 공통 스크립트 버전 */
mon.version = '1.0.1';

if (!window.Promise) {
      window.Promise = Promise;
}

mon.type = require('../common/type');
mon.func = require('../common/function');
mon.array = require('../common/array');
mon.string = require('../common/string');
mon.url = require('../common/url');
mon.domain = require('../common/domain');
mon.validator = require('../common/validator');
mon.Popup = require('./popup');

var _mon = mon;
/**
 * 타 라이브러리 충돌방지를 위한 alias 처리 (사용법 : var mon = mon.noConflict();)
 * @memberof mon
 * @returns {object} mon 객체
 */
mon.noConflict = function() {
    return _mon;
};

/**
 * 네임스페이스 객체를 가져온다. (존재하지 않으면 생성)
 * @memberof mon
 * @param {string} _namespace 네임스페이스 문자
 * @returns {object} 호출된 네임스페이스 객체
 */
mon.getNameSpace = function(_namespace) {
    var parts = _namespace.split('.'),
        parent = mon,
        i;
    if (parts[0] === 'mon') {
        parts = parts.slice(1);
    }
    for (i = 0; i < parts.length; i += 1) {
        // 프로퍼티가 존재하지 않으면 생성한다.
        if (typeof parent[parts[i]] === 'undefined') {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
};

module.exports = mon;

/**
 * mon console image
 */
(function() {
    if (mon.browser.chrome && mon.domain.getLocation() !== 'live') {
        console.log('%cmon is ready!', 'color: #f60; font-family: sans-serif; font-size: 1.5em; font-weight: bolder; text-shadow: #000 1px 1px;');
        console.log('%c(※ 이 문구는 테스트/검수 서버에서만 노출 됩니다.)', 'color: #f60');
        var image = new Image();
        var url = 'http://contents.albamon.kr/monimg/gnb/bi/thumbnail_logo_02.png';
        image.onload = function() {
            var consoleStyle = [
                'font-size: 1px;',
                'line-height: ' + this.height + 'px;',
                'padding: ' + this.width * .3 + 'px ' + this.height * .5 + 'px;',
                'background-size: ' + this.width + 'px ' + this.height + 'px;',
                'background: url(' + url + ');'
            ].join(' ');
            console.log('%c', consoleStyle);
        };
        image.src = url;
    }
})();
