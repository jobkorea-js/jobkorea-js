# JKMON JS

이 곳은 잡코리아 내부 개발의 자바스크립트 표준화를 위한 공간입니다.

- - -
JKMON JS는 (JOBKOREA + albamon) javascript 의 합성어로 잡코리아 사내 자바스크립트 표준화를 위한 프레임워크명입니다.
- - -

## 디렉토리 정보

depth1|depth2|설명
---|---|---
[demo](https://bitbucket.org/jobkorea-js/jobkorea-js/src/46ac48514cec631d5e4123d7c29f330f12d092ea/demo/?at=master)||데모 파일 경로
[dist](https://bitbucket.org/jobkorea-js/jobkorea-js/src/46ac48514cec631d5e4123d7c29f330f12d092ea/dist/?at=master)||배포 파일 경로
|[mon.pc.min.js](https://bitbucket.org/jobkorea-js/jobkorea-js/src/46ac48514cec631d5e4123d7c29f330f12d092ea/dist/mon.pc.min.js?at=master)|알바몬 PC 공통 JS 파일
|[mon.m.min.js](#)|알바몬 M 공통 JS 파일
|[jk.pc.min.js](#)|잡코리아 PC 공통 JS 파일
|[jk.m.min.js](#)|잡코리아 M 공통 JS 파일
[src](https://bitbucket.org/jobkorea-js/jobkorea-js/src/46ac48514cec631d5e4123d7c29f330f12d092ea/src/?at=master)||모듈 파일 경로
[.eslintrc.js](https://bitbucket.org/jobkorea-js/jobkorea-js/src/c595f1b47db03da29b644cd067eec76ca60a1248/.eslintrc.js?at=master)||ESLint 설정 파일
[webpack.config.js](https://bitbucket.org/jobkorea-js/jobkorea-js/src/c595f1b47db03da29b644cd067eec76ca60a1248/webpack.config.js?at=master)||webpack 설정 파일



## [JKMON JS 가이드](https://bitbucket.org/jobkorea-js/jobkorea-js/wiki/Home)

## JKMON JS API 가이드

* [알바몬 PC API 가이드](https://jobkorea-js.bitbucket.io/albamon/pc/)
* [알바몬 M API 가이드](https://jobkorea-js.bitbucket.io/albamon/m/)
* [잡코리아 PC API 가이드](#)
* [잡코리아 M API 가이드](#)

## 현재버전 1.0.1

[버젼 변경 정보](#)
