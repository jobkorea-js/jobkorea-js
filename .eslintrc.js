module.exports = {
    "env": {
        "browser": true,
        "jquery": true
    },
    "globals": {
        "window": true
    },
  "rules": {
        // Possible Errors
        "no-console": 1,
        // Best Practices
        "curly": [2, "multi-line"],
        "no-multi-spaces": 2,
        "no-new-wrappers": 2,
        "no-with": 2,
        // Variables
        "no-undef": 1,
        "no-unused-vars": [1, {"args": "none"}],
        // Stylistic Issues
        "brace-style": 1,
        "comma-dangle": [1, "never"],
        "indent": [1, 4, {"SwitchCase": 1}],
        "no-new-object": 1,
        "semi": 1
    }
};
