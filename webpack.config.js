var webpack = require('webpack');

module.exports = [
    // /**
    //  * 잡코리아 (PC용)
    //  */
    // { 
    //     name: 'jobkorea.pc',
    //     context: __dirname + '/sources', // 모듈 파일 폴더
    //     entry: { // 엔트리 파일 목록
    //         'jk.pc': './src/jk/pc/index.pc.js'
    //     },
    //     output: {
    //         path: __dirname + '/sources/dist', // 번들 파일 폴더
    //         filename: '[name].js' // 번들 파일 이름 규칙
    //         , library: 'jk' // 담을 전역변수명
    //     }
    // },
    // /**
    //  * 잡코리아 (모바일용)
    //  */
    // {
    //     name: 'jobkorea.mobile',
    //     context: __dirname + '/sources', // 모듈 파일 폴더
    //     entry: { // 엔트리 파일 목록
    //         'jk.m': './src/jk/m/index.m.js'
    //     },
    //     output: {
    //         path: __dirname + '/sources/dist', // 번들 파일 폴더
    //         filename: '[name].js' // 번들 파일 이름 규칙
    //         , library: 'jk' // 담을 전역변수명
    //     }
    // },
    // /**
    //  * 잡코리아 (PC용-압축버전)
    //  */
    // { 
    //     name: 'jobkorea.pc.min',
    //     context: __dirname + '/sources', // 모듈 파일 폴더
    //     entry: { // 엔트리 파일 목록
    //         'jk.pc': './src/jk/pc/index.pc.js'
    //     },
    //     output: {
    //         path: __dirname + '/sources/dist', // 번들 파일 폴더
    //         filename: '[name].min.js' // 번들 파일 이름 규칙
    //         , library: 'jk' // 담을 전역변수명
    //     }, 
    //     plugins: [
    //         new webpack.optimize.UglifyJsPlugin({
    //             compress: {
    //                 warnings: false
    //             }
    //         })
    //     ]
    // },
    // /**
    //  * 잡코리아 (모바일용-압축버전)
    //  */
    // {
    //     name: 'jobkorea.mobile.min',
    //     context: __dirname + '/sources', // 모듈 파일 폴더
    //     entry: { // 엔트리 파일 목록
    //         'jk.m': './src/jk/m/index.m.js'
    //     },
    //     output: {
    //         path: __dirname + '/sources/dist', // 번들 파일 폴더
    //         filename: '[name].min.js' // 번들 파일 이름 규칙
    //         , library: 'jk' // 담을 전역변수명
    //     }, plugins: [
    //         new webpack.optimize.UglifyJsPlugin({
    //             compress: {
    //                 warnings: false
    //             }
    //         })
    //     ]
    // },
    /**
     * 알바몬 (PC용)
     */
    {
        name: 'albamon.pc',
        context: __dirname + '/sources', // 모듈 파일 폴더
        entry: { // 엔트리 파일 목록
            'mon.pc': './src/mon/pc/index.pc.js'
        },
        output: {
            path: __dirname + '/sources/dist', // 번들 파일 폴더
            filename: '[name].js' // 번들 파일 이름 규칙
            , library: 'mon' // 담을 전역변수명
        }
    },
    /**
     * 알바몬 (모바일용)
     */
    {
        name: 'albamon.mobile',
        context: __dirname + '/sources', // 모듈 파일 폴더
        entry: { // 엔트리 파일 목록
            'mon.m': './src/mon/m/index.m.js'
        },
        output: {
            path: __dirname + '/sources/dist', // 번들 파일 폴더
            filename: '[name].js' // 번들 파일 이름 규칙
            , library: 'mon' // 담을 전역변수명
        }
    },
    /**
     * 알바몬 (PC용-압축버전)
     */
    {
        name: 'albamon.pc.min',
        context: __dirname + '/sources', // 모듈 파일 폴더
        entry: { // 엔트리 파일 목록
            'mon.pc': './src/mon/pc/index.pc.js'
        },
        output: {
            path: __dirname + '/sources/dist', // 번들 파일 폴더
            filename: '[name].min.js' // 번들 파일 이름 규칙
            , library: 'mon' // 담을 전역변수명
        }, 
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            })
        ]
    },
    /**
     * 알바몬 (모바일용-압축버전)
     */
    {
        name: 'albamon.mobile.min',
        context: __dirname + '/sources', // 모듈 파일 폴더
        entry: { // 엔트리 파일 목록
            'mon.m': './src/mon/m/index.m.js'
        },
        output: {
            path: __dirname + '/sources/dist', // 번들 파일 폴더
            filename: '[name].min.js' // 번들 파일 이름 규칙
            , library: 'mon' // 담을 전역변수명
        }, 
        plugins: [
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            })
        ]
    }
];